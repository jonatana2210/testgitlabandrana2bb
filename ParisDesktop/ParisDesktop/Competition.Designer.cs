﻿namespace ParisDesktop
{
    partial class Competition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fermerbutton = new System.Windows.Forms.Button();
            this.panelcompetition = new System.Windows.Forms.Panel();
            this.panelcompetition.SuspendLayout();
            this.SuspendLayout();
            // 
            // fermerbutton
            // 
            this.fermerbutton.Location = new System.Drawing.Point(0, 0);
            this.fermerbutton.Name = "fermerbutton";
            this.fermerbutton.Size = new System.Drawing.Size(67, 16);
            this.fermerbutton.TabIndex = 1;
            this.fermerbutton.Text = "X";
            this.fermerbutton.UseVisualStyleBackColor = true;
            this.fermerbutton.Click += new System.EventHandler(this.fermerbutton_Click);
            // 
            // panelcompetition
            // 
            this.panelcompetition.AutoScroll = true;
            this.panelcompetition.AutoScrollMinSize = new System.Drawing.Size(0, 3000);
            this.panelcompetition.Controls.Add(this.fermerbutton);
            this.panelcompetition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelcompetition.Location = new System.Drawing.Point(0, 0);
            this.panelcompetition.Name = "panelcompetition";
            this.panelcompetition.Size = new System.Drawing.Size(573, 377);
            this.panelcompetition.TabIndex = 2;
            this.panelcompetition.Paint += new System.Windows.Forms.PaintEventHandler(this.panelcompetition_Paint);
            // 
            // Competition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.ClientSize = new System.Drawing.Size(573, 377);
            this.Controls.Add(this.panelcompetition);
            this.Name = "Competition";
            this.Text = "Competition";
            this.Load += new System.EventHandler(this.Competition_Load);
            this.panelcompetition.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button fermerbutton;
        private System.Windows.Forms.Panel panelcompetition;
    }
}