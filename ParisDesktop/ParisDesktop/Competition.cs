﻿using ParisDesktop.Model;
using ParisDesktop.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
 
namespace ParisDesktop
{
    public partial class Competition : Form
    {
        string idcompetition;
        Form1 form1;
        public Competition(string idcompetition, Form1 form1)
        {
            InitializeComponent();
            this.idcompetition=idcompetition;
            this.form1 = form1;
            Console.WriteLine("nom competition " + idcompetition);
        }

        public string generateID()
        {
            return Guid.NewGuid().ToString("N");
        }
        private async void creerCardMilieu(int i,Match match)
        {
            //Api paris
            var listeparisjson = await PariApi.GetPariParMatch(match._id);
            List<Pari> parisliste = PariApi.getListeParisParMatch(listeparisjson);
            Random rnd = new Random();
            //int randomuniqueid = rnd.Next(126, 225);
            string randomuniqueid =generateID();
            // 
            // detailsMatch
            // 
            Button detailsMatch = new Button();
            detailsMatch.BackColor = System.Drawing.Color.Crimson;
            detailsMatch.FlatAppearance.BorderSize = 0;
            detailsMatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            detailsMatch.ForeColor = System.Drawing.Color.White;
            detailsMatch.Location = new System.Drawing.Point(455, 6);
            detailsMatch.Name = match._id+"/"+"detailsMatch" + randomuniqueid;
            detailsMatch.Size = new System.Drawing.Size(75, 23);
            detailsMatch.TabIndex = 13;
            detailsMatch.Text = "voir detail";
            detailsMatch.UseVisualStyleBackColor = false;
            detailsMatch.Click += new System.EventHandler(form1.detailsMatch_Click);
            // 
            // label1
            // 
            Label label1 = new Label();
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(356, 14);
            label1.Name = match._id + "/" + "datematch" + randomuniqueid;
            label1.Size = new System.Drawing.Size(65, 13);
            label1.TabIndex = 12;
            label1.Text = match.date.ToString("dd/MM/yyyy");
            // 
            // panelcote2
            // 
            Panel panelcote2 = new Panel();
            panelcote2.BackColor = ColorTranslator.FromHtml("#ffcc00");

            panelcote2.Location = new System.Drawing.Point(366, 39);
            panelcote2.Name = match._id + "/" + "panelcote2" + randomuniqueid;
            panelcote2.Size = new System.Drawing.Size(164, 40);
            panelcote2.TabIndex = 11;
            // 
            // label2
            // 

            Label label2 = new Label();
            label2.AutoSize = true;
            label2.ForeColor = System.Drawing.Color.Black;
            label2.Location = new System.Drawing.Point(121, 14);
            label2.Name = match._id + "/" + "valeurcote2a" + randomuniqueid;
            label2.Size = new System.Drawing.Size(13, 13);
            label2.TabIndex = 7;
            if(parisliste[0]!=null)
            label2.Text = parisliste[0].cote.ToString() ;
            // 
            // cote2
            // 
            Button cote2 = new Button();
            cote2.BackColor = System.Drawing.Color.White;
            cote2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            cote2.Location = new System.Drawing.Point(0, 0);
            cote2.Name = match._id + "/"  +parisliste[0]._id.ToString()+"/" + "cote2" + randomuniqueid;
            cote2.Size = new System.Drawing.Size(115, 40);
            cote2.TabIndex = 5;
            cote2.Text = "2";
            cote2.UseVisualStyleBackColor = false;
            cote2.Click += new System.EventHandler(form1.cote1_Click);

           


            // 
            // panelcotenull
            // 
            Panel panelcotenull = new Panel();
            panelcotenull.BackColor = ColorTranslator.FromHtml("#ffcc00");
            panelcotenull.Location = new System.Drawing.Point(186, 39);
            panelcotenull.Name = match._id + "/" + "panelcotenull" + randomuniqueid;
            panelcotenull.Size = new System.Drawing.Size(164, 40);
            panelcotenull.TabIndex = 10;
            // 
            // label3
            // 
            Label label3 = new Label();
            label3.AutoSize = true;
            label3.ForeColor = System.Drawing.Color.Black;
            label3.Location = new System.Drawing.Point(121, 14);
            label3.Name = match._id + "/" + "valeurcotenulla" + randomuniqueid;
            label3.Size = new System.Drawing.Size(13, 13);
            label3.TabIndex = 7;
            if (parisliste[1] != null)
                label3.Text = parisliste[1].cote.ToString();
            // 
            // cotenull
            // 
            Button cotenull = new Button();
            cotenull.BackColor = System.Drawing.Color.White;
            cotenull.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            cotenull.Location = new System.Drawing.Point(0, 0);
            cotenull.Name = match._id + "/" + parisliste[1]._id.ToString() + "/" + "cotenull" + randomuniqueid;
            cotenull.Size = new System.Drawing.Size(115, 40);
            cotenull.TabIndex = 5;
            cotenull.Text = "X";
            cotenull.UseVisualStyleBackColor = false;
            cotenull.Click += new System.EventHandler(form1.cote1_Click);
            // 
            // panelcote1
            //
            Panel panelcote1 = new Panel();
            panelcote1.BackColor = ColorTranslator.FromHtml("#ffcc00");

            panelcote1.Location = new System.Drawing.Point(3, 39);
            panelcote1.Name = match._id + "/" + "panelcote1" + randomuniqueid;
            panelcote1.Size = new System.Drawing.Size(164, 40);
            panelcote1.TabIndex = 9;
            // 
            // label5
            // 
            Label label5 = new Label();
            label5.AutoSize = true;
            label5.ForeColor = System.Drawing.Color.Black;
            label5.Location = new System.Drawing.Point(121, 14);
            label5.Name = match._id + "/" + "valeurcote1a" + randomuniqueid;
            label5.Size = new System.Drawing.Size(13, 13);
            label5.TabIndex = 7;
            if (parisliste[2] != null)
                label5.Text = parisliste[2].cote.ToString();
            // 
            // cote1
            // 
            Button cote1 = new Button();
            cote1.BackColor = System.Drawing.Color.White;
            cote1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            cote1.Location = new System.Drawing.Point(0, 0);
            cote1.Name = match._id + "/" + parisliste[2]._id.ToString() + "/" + "cote1" + randomuniqueid;
            cote1.Size = new System.Drawing.Size(115, 40);
            cote1.TabIndex = 5;
            cote1.Text = "1";
            cote1.UseVisualStyleBackColor = false;
            cote1.Click += new System.EventHandler(form1.cote1_Click);
            // 
            // label6
            //
            Label label6 = new Label();
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(109, 16);
            label6.Name = match._id + "/" + "1club" + randomuniqueid;
            label6.Size = new System.Drawing.Size(45, 13);
            label6.TabIndex = 4;
            label6.Text = match.equipe1.nom ;
            // 
            // label7
            // 
            Label label7 = new Label();
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(93, 16);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(10, 13);
            label7.TabIndex = 3;
            label7.Text = "-";
            // 
            // label8
            // 
            Label label8 = new Label();
            label8.AutoSize = true;
            label8.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label8.Location = new System.Drawing.Point(12, 15);
            label8.Name = match._id + "/" + "2club" + randomuniqueid;
            label8.Size = new System.Drawing.Size(75, 16);
            label8.TabIndex = 2;
            label8.Text = match.equipe2.nom;
            panelcote2.Controls.Add(label2);
            panelcote2.Controls.Add(cote2);
            panelcotenull.Controls.Add(label3);
            panelcotenull.Controls.Add(cotenull);
            panelcote1.Controls.Add(label5);
            panelcote1.Controls.Add(cote1);
            Panel panelcard = new Panel();
            panelcard.Anchor = System.Windows.Forms.AnchorStyles.None;
            panelcard.AutoSize = true;
            panelcard.BackColor = System.Drawing.Color.White;
            panelcard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            panelcard.Controls.Add(detailsMatch);
            panelcard.Controls.Add(label1);
            panelcard.Controls.Add(panelcote2);
            panelcard.Controls.Add(panelcotenull);
            panelcard.Controls.Add(panelcote1);
            panelcard.Controls.Add(label6);
            panelcard.Controls.Add(label7);
            panelcard.Controls.Add(label8);
            panelcard.Location = new System.Drawing.Point(10, i * 100);
            panelcard.Name = match._id+"/"+"cardmilieu" + randomuniqueid;
            panelcard.Size = new System.Drawing.Size(549, 90);
            panelcard.TabIndex = 20;
            this.panelcompetition.AutoScroll = true;
            this.panelcompetition.Controls.Add(panelcard);
        }
        private void fermerbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private async void Competition_Load(object sender, EventArgs e)
        {   /*if(nomcompetition.Contains("ligue1"))
            {*/

            var liste = await MatchApi.GetMatchParChampionnat(idcompetition);
            List<Match> matches = MatchApi.BeautifyJsonMatchParChampionnat(liste);
            for (int i = 0; i < matches.Count ; i++)
                    creerCardMilieu(i+1, matches[i]);
            //}
            
        }

        private void panel12_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panelcompetition_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
