﻿namespace ParisDesktop
{
    partial class DetailMatch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.paneldetail = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panelmilieudetail = new System.Windows.Forms.Panel();
            this.bas = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.boutonnon = new System.Windows.Forms.Button();
            this.cotenon = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.boutonoui = new System.Windows.Forms.Button();
            this.coteoui = new System.Windows.Forms.Label();
            this.heurematchdetail = new System.Windows.Forms.Label();
            this.datematchdetails = new System.Windows.Forms.Label();
            this.endroit = new System.Windows.Forms.Label();
            this.detailequipe2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.detailequipe1 = new System.Windows.Forms.Label();
            this.paneltitre = new System.Windows.Forms.Panel();
            this.championnat = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.fermerdetailsbouton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4.SuspendLayout();
            this.panelmilieudetail.SuspendLayout();
            this.bas.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.paneltitre.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // paneldetail
            // 
            // this.paneldetail.Controls.Add(panel4);
           // this.paneldetail.Controls.Add(panel3);
            this.paneldetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.paneldetail.Location = new System.Drawing.Point(0, 0);
            //this.paneldetail.Name = "paneldetail";
            this.paneldetail.Size = new System.Drawing.Size(573, 377);
            this.paneldetail.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panelmilieudetail);
            this.panel4.Controls.Add(this.paneltitre);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 56);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(573, 321);
            this.panel4.TabIndex = 5;
            // 
            // panelmilieudetail
            // 
            this.panelmilieudetail.BackColor = System.Drawing.Color.White;
            this.panelmilieudetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelmilieudetail.Controls.Add(this.bas);
            this.panelmilieudetail.Controls.Add(this.heurematchdetail);
            this.panelmilieudetail.Controls.Add(this.datematchdetails);
            this.panelmilieudetail.Controls.Add(this.endroit);
            this.panelmilieudetail.Controls.Add(this.detailequipe2);
            this.panelmilieudetail.Controls.Add(this.label3);
            this.panelmilieudetail.Controls.Add(this.detailequipe1);
            this.panelmilieudetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelmilieudetail.Location = new System.Drawing.Point(0, 48);
            this.panelmilieudetail.Name = "panelmilieudetail";
            this.panelmilieudetail.Size = new System.Drawing.Size(573, 273);
            this.panelmilieudetail.TabIndex = 1;
            this.panelmilieudetail.Paint += new System.Windows.Forms.PaintEventHandler(this.panelmilieudetail_Paint);
            // 
            // bas
            // 
            this.bas.Controls.Add(this.label2);
            this.bas.Controls.Add(this.panel6);
            this.bas.Controls.Add(this.panel5);
            this.bas.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bas.Location = new System.Drawing.Point(0, 124);
            this.bas.Name = "bas";
            this.bas.Size = new System.Drawing.Size(571, 147);
            this.bas.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(48, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(196, 21);
            this.label2.TabIndex = 2;
            this.label2.Text = "Les 2 équipes marquent :";
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.boutonnon);
            this.panel6.Controls.Add(this.cotenon);
            this.panel6.Location = new System.Drawing.Point(360, 71);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(164, 40);
            this.panel6.TabIndex = 1;
            // 
            // boutonnon
            // 
            this.boutonnon.Location = new System.Drawing.Point(-1, 0);
            this.boutonnon.Name = "boutonnon";
            this.boutonnon.Size = new System.Drawing.Size(115, 40);
            this.boutonnon.TabIndex = 1;
            this.boutonnon.Text = "non";
            this.boutonnon.UseVisualStyleBackColor = true;
            this.boutonnon.Click += new System.EventHandler(this.boutonnon_Click);
            // 
            // cotenon
            // 
            this.cotenon.AutoSize = true;
            this.cotenon.Location = new System.Drawing.Point(121, 14);
            this.cotenon.Name = "cotenon";
            this.cotenon.Size = new System.Drawing.Size(22, 13);
            this.cotenon.TabIndex = 1;
            this.cotenon.Text = "2,3";
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.boutonoui);
            this.panel5.Controls.Add(this.coteoui);
            this.panel5.Location = new System.Drawing.Point(52, 70);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(164, 40);
            this.panel5.TabIndex = 0;
            this.panel5.Paint += new System.Windows.Forms.PaintEventHandler(this.panel5_Paint);
            // 
            // boutonoui
            // 
            this.boutonoui.Location = new System.Drawing.Point(-1, 0);
            this.boutonoui.Name = "boutonoui";
            this.boutonoui.Size = new System.Drawing.Size(115, 40);
            this.boutonoui.TabIndex = 1;
            this.boutonoui.Text = "oui";
            this.boutonoui.UseVisualStyleBackColor = true;
            this.boutonoui.Click += new System.EventHandler(this.boutonoui_Click);
            // 
            // coteoui
            // 
            this.coteoui.AutoSize = true;
            this.coteoui.Location = new System.Drawing.Point(121, 14);
            this.coteoui.Name = "coteoui";
            this.coteoui.Size = new System.Drawing.Size(22, 13);
            this.coteoui.TabIndex = 1;
            this.coteoui.Text = "1,7";
            // 
            // heurematchdetail
            // 
            this.heurematchdetail.AutoSize = true;
            this.heurematchdetail.Location = new System.Drawing.Point(369, 72);
            this.heurematchdetail.Name = "heurematchdetail";
            this.heurematchdetail.Size = new System.Drawing.Size(34, 13);
            this.heurematchdetail.TabIndex = 5;
            this.heurematchdetail.Text = "22:00";
            // 
            // datematchdetails
            // 
            this.datematchdetails.AutoSize = true;
            this.datematchdetails.Location = new System.Drawing.Point(260, 72);
            this.datematchdetails.Name = "datematchdetails";
            this.datematchdetails.Size = new System.Drawing.Size(65, 13);
            this.datematchdetails.TabIndex = 4;
            this.datematchdetails.Text = "22/05/2021";
            // 
            // endroit
            // 
            this.endroit.AutoSize = true;
            this.endroit.Location = new System.Drawing.Point(108, 72);
            this.endroit.Name = "endroit";
            this.endroit.Size = new System.Drawing.Size(87, 13);
            this.endroit.TabIndex = 3;
            this.endroit.Text = "Angleterre londre";
            // 
            // detailequipe2
            // 
            this.detailequipe2.AutoSize = true;
            this.detailequipe2.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.detailequipe2.Location = new System.Drawing.Point(390, 27);
            this.detailequipe2.Name = "detailequipe2";
            this.detailequipe2.Size = new System.Drawing.Size(134, 24);
            this.detailequipe2.TabIndex = 2;
            this.detailequipe2.Text = "Real madrid";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(271, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(19, 25);
            this.label3.TabIndex = 1;
            this.label3.Text = "-";
            // 
            // detailequipe1
            // 
            this.detailequipe1.AutoSize = true;
            this.detailequipe1.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.detailequipe1.Location = new System.Drawing.Point(48, 26);
            this.detailequipe1.Name = "detailequipe1";
            this.detailequipe1.Size = new System.Drawing.Size(95, 24);
            this.detailequipe1.TabIndex = 0;
            this.detailequipe1.Text = "Chelsea";
            // 
            // paneltitre
            // 
            this.paneltitre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.paneltitre.Controls.Add(this.championnat);
            this.paneltitre.Dock = System.Windows.Forms.DockStyle.Top;
            this.paneltitre.Location = new System.Drawing.Point(0, 0);
            this.paneltitre.Name = "paneltitre";
            this.paneltitre.Size = new System.Drawing.Size(573, 48);
            this.paneltitre.TabIndex = 3;
            // 
            // championnat
            // 
            this.championnat.AutoSize = true;
            this.championnat.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.championnat.Location = new System.Drawing.Point(12, 17);
            this.championnat.Name = "championnat";
            this.championnat.Size = new System.Drawing.Size(139, 16);
            this.championnat.TabIndex = 0;
            this.championnat.Text = "Ligue  des champions";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.fermerdetailsbouton);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(573, 56);
            this.panel3.TabIndex = 4;
            // 
            // fermerdetailsbouton
            // 
            this.fermerdetailsbouton.Location = new System.Drawing.Point(12, 12);
            this.fermerdetailsbouton.Name = "fermerdetailsbouton";
            this.fermerdetailsbouton.Size = new System.Drawing.Size(43, 34);
            this.fermerdetailsbouton.TabIndex = 0;
            this.fermerdetailsbouton.Text = "X";
            this.fermerdetailsbouton.UseVisualStyleBackColor = true;
            this.fermerdetailsbouton.Click += new System.EventHandler(this.fermerdetailsbouton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(82, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "Détails du match";
            // 
            // DetailMatch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.ClientSize = new System.Drawing.Size(573, 377);
            this.Controls.Add(this.paneldetail);
            this.Name = "DetailMatch";
            this.Text = "DetailMatch";
            this.Load += new System.EventHandler(this.DetailMatch_Load);
            this.panel4.ResumeLayout(false);
            this.panelmilieudetail.ResumeLayout(false);
            this.panelmilieudetail.PerformLayout();
            this.bas.ResumeLayout(false);
            this.bas.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.paneltitre.ResumeLayout(false);
            this.paneltitre.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel paneldetail;
        private System.Windows.Forms.Button fermerdetailsbouton;
        private System.Windows.Forms.Panel paneltitre;
        private System.Windows.Forms.Label championnat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelmilieudetail;
        private System.Windows.Forms.Label heurematchdetail;
        private System.Windows.Forms.Label datematchdetails;
        private System.Windows.Forms.Label endroit;
        private System.Windows.Forms.Label detailequipe2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label detailequipe1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel bas;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label coteoui;
        private System.Windows.Forms.Button boutonoui;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button boutonnon;
        private System.Windows.Forms.Label cotenon;
        private System.Windows.Forms.Label label2;
    }
}