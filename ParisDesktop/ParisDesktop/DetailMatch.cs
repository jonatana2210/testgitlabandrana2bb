﻿using ParisDesktop.Model;
using ParisDesktop.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParisDesktop
{
    public partial class DetailMatch : Form
    {
        Form1 form1;
        string idMatch;
        public DetailMatch(Form1 form1, string idMatch)
        {
            InitializeComponent();
            this.form1 = form1;
            this.idMatch = idMatch;
            Console.WriteLine(this.idMatch + " id match ");
            
        }
        private  async void DetailMatch_Load(object sender, EventArgs e)
        {
            var details = await MatchApi.GetMatchById(idMatch);
            Match match = MatchApi.GetMatch(details);
            this.creerDetailsAffichage(match);

        }
        public string generateID()
        {
            return Guid.NewGuid().ToString("N");
        }

        private void fermerdetailsbouton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void boutonnon_Click(object sender, EventArgs e)
        {
            form1.cote1_Click(sender,e);
        }

        private void boutonoui_Click(object sender, EventArgs e)
        {
            form1.cote1_Click(sender, e);
        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panelmilieudetail_Paint(object sender, PaintEventArgs e)
        {

        }
        public async void creerDetailsAffichage(Match match)
        {
            //Api paris
            var listeparisjson = await PariApi.GetPariParMatch(match._id);
            List<Pari> parisliste = PariApi.getListeParisParMatch(listeparisjson);
            string randomuniqueid = generateID();
            // 
            // fermerdetailsbouton
            // 
            Button fermerdetailsbouton = new Button();
            fermerdetailsbouton.Location = new System.Drawing.Point(12, 12);
            fermerdetailsbouton.Name = "fermerdetailsbouton";
            fermerdetailsbouton.Size = new System.Drawing.Size(43, 34);
            fermerdetailsbouton.TabIndex = 0;
            fermerdetailsbouton.Text = "X";
            fermerdetailsbouton.UseVisualStyleBackColor = true;
            fermerdetailsbouton.Click += new System.EventHandler(this.fermerdetailsbouton_Click);
            // 
            // label1
            // 
            Label label1 = new Label();
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.Location = new System.Drawing.Point(82, 22);
            label1.Name = "label1"+ randomuniqueid;
            label1.Size = new System.Drawing.Size(148, 24);
            label1.TabIndex = 2;
            label1.Text = "Détails du match";

            // 
            // championnat
            // 
            Label championnat = new Label();
            championnat.AutoSize = true;
            championnat.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            championnat.Location = new System.Drawing.Point(12, 17);
            championnat.Name = "championnat" + randomuniqueid;
            championnat.Size = new System.Drawing.Size(139, 16);
            championnat.TabIndex = 0;
            championnat.Text = match.championnat.nom;
            // 
            // panel3
            // 
            Panel panel3 = new Panel();
            panel3.Controls.Add(fermerdetailsbouton);
            panel3.Controls.Add(label1);
            panel3.Dock = System.Windows.Forms.DockStyle.Top;
            panel3.Location = new System.Drawing.Point(0, 0);
            panel3.Name = "panel3" + randomuniqueid;
            panel3.Size = new System.Drawing.Size(573, 56);
            panel3.TabIndex = 4;

            // 
            // paneltitre
            // 
            Panel paneltitre = new Panel();
            paneltitre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            paneltitre.Controls.Add(championnat);
            paneltitre.Dock = System.Windows.Forms.DockStyle.Top;
            paneltitre.Location = new System.Drawing.Point(0, 0);
            paneltitre.Name = "paneltitre" + randomuniqueid;
            paneltitre.Size = new System.Drawing.Size(573, 48);
            paneltitre.TabIndex = 3;

            // 
            // boutonoui
            //
            Button boutonoui = new Button();
            boutonoui.BackColor = System.Drawing.Color.White;
            boutonoui.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            boutonoui.Location = new System.Drawing.Point(-1, 0);
            boutonoui.Name = match._id + "/" + parisliste[3]._id.ToString() + "/" + "boutonoui" + randomuniqueid;
            boutonoui.Size = new System.Drawing.Size(115, 40);
            boutonoui.TabIndex = 1;
            boutonoui.Text = "oui";
            boutonoui.UseVisualStyleBackColor = true;
            boutonoui.Click += new System.EventHandler(form1.cote1_Click);
            // 
            // coteoui
            //
            Label coteoui = new Label();
            coteoui.AutoSize = true;
            coteoui.Location = new System.Drawing.Point(121, 14);
            coteoui.Name = "coteoui" + randomuniqueid;
            coteoui.Size = new System.Drawing.Size(22, 13);
            coteoui.TabIndex = 1;
            if (parisliste[3] != null)
                coteoui.Text = parisliste[3].cote.ToString();
            // 
            // heurematchdetail
            // 
            Label heurematchdetail = new Label();
            heurematchdetail.AutoSize = true;
            heurematchdetail.Location = new System.Drawing.Point(369, 72);
            heurematchdetail.Name = "heurematchdetail" + randomuniqueid;
            heurematchdetail.Size = new System.Drawing.Size(34, 13);
            heurematchdetail.TabIndex = 5;
            heurematchdetail.Text = "22:00";
            // 
            // datematchdetails
            // 
            Label datematchdetails = new Label();
            datematchdetails.AutoSize = true;
            datematchdetails.Location = new System.Drawing.Point(260, 72);
            datematchdetails.Name = "datematchdetails" + randomuniqueid;
            datematchdetails.Size = new System.Drawing.Size(65, 13);
            datematchdetails.TabIndex = 4;
            datematchdetails.Text = match.date.ToString("dd/MM/yyyy") ;
            // 
            // endroit
            // 
            Label endroit = new Label();
            endroit.AutoSize = true;
            endroit.Location = new System.Drawing.Point(108, 72);
            endroit.Name = "endroit" + randomuniqueid;
            endroit.Size = new System.Drawing.Size(87, 13);
            endroit.TabIndex = 3;
            endroit.Text = match.endroit;
            // 
            // detailequipe2
            //
            Label detailsequipe2 = new Label();
            detailequipe2.AutoSize = true;
            detailequipe2.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            detailequipe2.Location = new System.Drawing.Point(390, 27);
            detailequipe2.Name = "detailequipe2" + randomuniqueid;
            detailequipe2.Size = new System.Drawing.Size(134, 24);
            detailequipe2.TabIndex = 2;
            detailequipe2.Text = match.equipe2.nom;
            // 
            // label3
            //
            Label label3 = new Label();
            label3.AutoSize = true;
            label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label3.Location = new System.Drawing.Point(271, 25);
            label3.Name = "label3" + randomuniqueid;
            label3.Size = new System.Drawing.Size(19, 25);
            label3.TabIndex = 1;
            label3.Text = "-";
            // 
            // detailequipe1
            // 
            Label detailsequipe1 = new Label();
            detailequipe1.AutoSize = true;
            detailequipe1.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            detailequipe1.Location = new System.Drawing.Point(48, 26);
            detailequipe1.Name = "detailequipe1" + randomuniqueid;
            detailequipe1.Size = new System.Drawing.Size(95, 24);
            detailequipe1.TabIndex = 0;
            detailequipe1.Text = match.equipe1.nom;

            // 
            // panel5
            // 
            Panel panel5 = new Panel();
            panel5.BackColor = ColorTranslator.FromHtml("#ffcc00");
            panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            panel5.Controls.Add(boutonoui);
            panel5.Controls.Add(coteoui);
            panel5.Location = new System.Drawing.Point(52, 70);
            panel5.Name = "panel5" + randomuniqueid;
            panel5.Size = new System.Drawing.Size(164, 40);
            panel5.TabIndex = 0;
            panel5.Paint += new System.Windows.Forms.PaintEventHandler(this.panel5_Paint);

            // 
            // boutonnon
            // 
            Button boutonnon = new Button();
            boutonnon.BackColor = System.Drawing.Color.White;
            boutonnon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            boutonnon.Location = new System.Drawing.Point(-1, 0);
            boutonnon.Name = match._id + "/" + parisliste[4]._id.ToString() + "/" + "boutonnon" + randomuniqueid;
            boutonnon.Size = new System.Drawing.Size(115, 40);
            boutonnon.TabIndex = 1;
            boutonnon.Text = "non" ;
            boutonnon.UseVisualStyleBackColor = true;
            boutonnon.Click += new System.EventHandler(form1.cote1_Click);
            // 
            // cotenon
            // 
            Label cotenon = new Label();
            cotenon.AutoSize = true;
            cotenon.Location = new System.Drawing.Point(121, 14);
            cotenon.Name = "cotenon" + randomuniqueid;
            cotenon.Size = new System.Drawing.Size(22, 13);
            cotenon.TabIndex = 1;
            if (parisliste[4] != null)
                cotenon.Text = parisliste[4].cote.ToString();

            // 
            // panel6
            // 
            Panel panel6 = new Panel();
            panel6.BackColor = ColorTranslator.FromHtml("#ffcc00");
            panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            panel6.Controls.Add(boutonnon);
            panel6.Controls.Add(cotenon);
            panel6.Location = new System.Drawing.Point(360, 71);
            panel6.Name = "panel6" + randomuniqueid;
            panel6.Size = new System.Drawing.Size(164, 40);
            panel6.TabIndex = 1;

            // 
            // label2
            // 
            Label label2 = new Label();
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label2.Location = new System.Drawing.Point(48, 18);
            label2.Name = "label2" + randomuniqueid;
            label2.Size = new System.Drawing.Size(196, 21);
            label2.TabIndex = 2;
            label2.Text = "Les 2 équipes marquent :";

            // bas
            //
            Panel bas = new Panel();
            bas.Controls.Add(label2);
            bas.Controls.Add(panel6);
            bas.Controls.Add(panel5);
            bas.Dock = System.Windows.Forms.DockStyle.Bottom;
            bas.Location = new System.Drawing.Point(0, 124);
            bas.Name = "bas" + randomuniqueid;
            bas.Size = new System.Drawing.Size(571, 147);
            bas.TabIndex = 6;

            // 
            // panelmilieudetail
            //
            Panel panelmilieudetail = new Panel();
            panelmilieudetail.BackColor = System.Drawing.Color.White;
            panelmilieudetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            panelmilieudetail.Controls.Add(bas);
            panelmilieudetail.Controls.Add(heurematchdetail);
            panelmilieudetail.Controls.Add(datematchdetails);
            panelmilieudetail.Controls.Add(endroit);
            panelmilieudetail.Controls.Add(detailequipe2);
            panelmilieudetail.Controls.Add(label3);
            panelmilieudetail.Controls.Add(detailequipe1);
            panelmilieudetail.Dock = System.Windows.Forms.DockStyle.Fill;
            panelmilieudetail.Location = new System.Drawing.Point(0, 48);
            panelmilieudetail.Name = "panelmilieudetail" + randomuniqueid;
            panelmilieudetail.Size = new System.Drawing.Size(573, 273);
            panelmilieudetail.TabIndex = 1;
            panelmilieudetail.Paint += new System.Windows.Forms.PaintEventHandler(this.panelmilieudetail_Paint);

            // 
            // panel4
            // 
            Panel panel4 = new Panel();
            panel4.Controls.Add(panelmilieudetail);
            panel4.Controls.Add(paneltitre);
            panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            panel4.Location = new System.Drawing.Point(0, 56);
            panel4.Name = "panel4" + randomuniqueid;
            panel4.Size = new System.Drawing.Size(573, 321);
            panel4.TabIndex = 5;
            // paneldetail
            // 
            //Panel paneldetail = new Panel();
            this.paneldetail.Controls.Add(panel4);
            this.paneldetail.Controls.Add(panel3); 
            this.paneldetail.Name = "paneldetail" + randomuniqueid;
            /* this.paneldetail.Dock = System.Windows.Forms.DockStyle.Fill;
             this.paneldetail.Location = new System.Drawing.Point(0, 0);

             this.paneldetail.Size = new System.Drawing.Size(573, 377);
             this.paneldetail.TabIndex = 0;*/

        }
    }
}
