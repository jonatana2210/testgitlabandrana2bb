﻿namespace ParisDesktop
{
    partial class DetailsHistorique
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.fermerbutton = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.pariPanierBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pariPanierBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // fermerbutton
            // 
            this.fermerbutton.Location = new System.Drawing.Point(3, -1);
            this.fermerbutton.Name = "fermerbutton";
            this.fermerbutton.Size = new System.Drawing.Size(67, 16);
            this.fermerbutton.TabIndex = 3;
            this.fermerbutton.Text = "X";
            this.fermerbutton.UseVisualStyleBackColor = true;
            this.fermerbutton.Click += new System.EventHandler(this.fermerbutton_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(3, 39);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(542, 287);
            this.dataGridView2.TabIndex = 4;
            // 
            // pariPanierBindingSource
            // 
            this.pariPanierBindingSource.DataSource = typeof(ParisDesktop.Model.PariPanier);
            // 
            // DetailsHistorique
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(557, 338);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.fermerbutton);
            this.Name = "DetailsHistorique";
            this.Text = "DetailsHistorique";
            this.Load += new System.EventHandler(this.DetailsHistorique_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pariPanierBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button fermerbutton;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.BindingSource pariPanierBindingSource;
    }
}