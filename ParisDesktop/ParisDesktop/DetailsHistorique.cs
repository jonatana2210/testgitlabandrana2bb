﻿using ParisDesktop.Model;
using ParisDesktop.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParisDesktop
{
    public partial class DetailsHistorique : Form
    {
        string idpanier;
        Form1 form1;
        public DetailsHistorique(Form1 form1, string idpari)
        {
            this.idpanier = idpari;
            this.form1 = form1;
            InitializeComponent();
        }

        private void fermerbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private async  void DetailsHistorique_Load(object sender, EventArgs e)
        {
            var liste = await PariApi.GetPariParPanier(this.idpanier);
            List<PariPanier> pari = PariApi.getListePariParPanier(liste);
            var bind = pari.Select(a => new
            {
                equipe1 = a.pari.match.equipe1.nom,
                equipe2 = a.pari.match.equipe2.nom,
                Valeur = a.pari.valeur,
                Cote = a.pari.cote,
                Mise = a.pari.mise
            }).ToList();
            dataGridView2.DataSource = bind ;
        }
    }
}
