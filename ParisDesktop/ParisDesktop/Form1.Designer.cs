﻿using System.Drawing;

namespace ParisDesktop
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.header = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.argent = new System.Windows.Forms.Label();
            this.solde = new System.Windows.Forms.Label();
            this.soldetexte = new System.Windows.Forms.Label();
            this.deconnexion = new System.Windows.Forms.Button();
            this.profil = new System.Windows.Forms.Button();
            this.motdepasselogin = new System.Windows.Forms.TextBox();
            this.login = new System.Windows.Forms.TextBox();
            this.inscription = new System.Windows.Forms.Button();
            this.connexion = new System.Windows.Forms.Button();
            this.menuHaut = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.paneldepot = new System.Windows.Forms.Panel();
            this.validerdepot = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.depot = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textRecherche = new System.Windows.Forms.TextBox();
            this.recherche = new System.Windows.Forms.Button();
            this.contenu = new System.Windows.Forms.Panel();
            this.milieu = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuDroite = new System.Windows.Forms.Panel();
            this.validation = new System.Windows.Forms.Panel();
            this.misetotalmultiple = new System.Windows.Forms.Label();
            this.gainpotentielmultiple = new System.Windows.Forms.Label();
            this.placementparis = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.misetotalvaleur = new System.Windows.Forms.Label();
            this.misesomme = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.menuDroiteContenu = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.boutonmultiple = new System.Windows.Forms.Button();
            this.boutonsimple = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.supprimertous = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.menuGauche = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.sidepanel = new System.Windows.Forms.Panel();
            this.football = new System.Windows.Forms.Button();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel12.SuspendLayout();
            this.menuHaut.SuspendLayout();
            this.panel13.SuspendLayout();
            this.paneldepot.SuspendLayout();
            this.panel1.SuspendLayout();
            this.contenu.SuspendLayout();
            this.milieu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuDroite.SuspendLayout();
            this.validation.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.SuspendLayout();
            // 
            // header
            // 
            this.header.BackColor = System.Drawing.Color.White;
            this.header.Controls.Add(this.pictureBox2);
            this.header.Controls.Add(this.panel12);
            this.header.Dock = System.Windows.Forms.DockStyle.Top;
            this.header.Location = new System.Drawing.Point(0, 0);
            this.header.Name = "header";
            this.header.Size = new System.Drawing.Size(956, 72);
            this.header.TabIndex = 0;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::ParisDesktop.Properties.Resources.winbet;
            this.pictureBox2.Location = new System.Drawing.Point(25, 10);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(100, 50);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 12;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.argent);
            this.panel12.Controls.Add(this.solde);
            this.panel12.Controls.Add(this.soldetexte);
            this.panel12.Controls.Add(this.deconnexion);
            this.panel12.Controls.Add(this.profil);
            this.panel12.Controls.Add(this.motdepasselogin);
            this.panel12.Controls.Add(this.login);
            this.panel12.Controls.Add(this.inscription);
            this.panel12.Controls.Add(this.connexion);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel12.Location = new System.Drawing.Point(242, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(714, 72);
            this.panel12.TabIndex = 5;
            // 
            // argent
            // 
            this.argent.AutoSize = true;
            this.argent.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.argent.ForeColor = System.Drawing.Color.Black;
            this.argent.Location = new System.Drawing.Point(583, 27);
            this.argent.Name = "argent";
            this.argent.Size = new System.Drawing.Size(29, 21);
            this.argent.TabIndex = 11;
            this.argent.Text = "Ar";
            this.argent.Visible = false;
            // 
            // solde
            // 
            this.solde.AutoSize = true;
            this.solde.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.solde.ForeColor = System.Drawing.Color.Black;
            this.solde.Location = new System.Drawing.Point(497, 27);
            this.solde.Name = "solde";
            this.solde.Size = new System.Drawing.Size(55, 21);
            this.solde.TabIndex = 10;
            this.solde.Text = "10000";
            this.solde.Visible = false;
            this.solde.Click += new System.EventHandler(this.solde_Click);
            // 
            // soldetexte
            // 
            this.soldetexte.AutoSize = true;
            this.soldetexte.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.soldetexte.ForeColor = System.Drawing.Color.Black;
            this.soldetexte.Location = new System.Drawing.Point(392, 25);
            this.soldetexte.Name = "soldetexte";
            this.soldetexte.Size = new System.Drawing.Size(102, 21);
            this.soldetexte.TabIndex = 9;
            this.soldetexte.Text = "votre solde :";
            this.soldetexte.Visible = false;
            // 
            // deconnexion
            // 
            this.deconnexion.Location = new System.Drawing.Point(651, 15);
            this.deconnexion.Name = "deconnexion";
            this.deconnexion.Size = new System.Drawing.Size(57, 43);
            this.deconnexion.TabIndex = 7;
            this.deconnexion.Text = "log out";
            this.deconnexion.UseVisualStyleBackColor = true;
            this.deconnexion.Visible = false;
            this.deconnexion.Click += new System.EventHandler(this.deconnexion_Click);
            // 
            // profil
            // 
            this.profil.Location = new System.Drawing.Point(318, 17);
            this.profil.Name = "profil";
            this.profil.Size = new System.Drawing.Size(57, 43);
            this.profil.TabIndex = 6;
            this.profil.Text = "profil";
            this.profil.UseVisualStyleBackColor = true;
            this.profil.Visible = false;
            this.profil.Click += new System.EventHandler(this.profil_Click);
            // 
            // motdepasselogin
            // 
            this.motdepasselogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.motdepasselogin.Location = new System.Drawing.Point(158, 19);
            this.motdepasselogin.Name = "motdepasselogin";
            this.motdepasselogin.PasswordChar = '*';
            this.motdepasselogin.Size = new System.Drawing.Size(143, 29);
            this.motdepasselogin.TabIndex = 5;
            this.motdepasselogin.Text = "textBox1";
            // 
            // login
            // 
            this.login.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.login.Location = new System.Drawing.Point(0, 19);
            this.login.Name = "login";
            this.login.Size = new System.Drawing.Size(143, 29);
            this.login.TabIndex = 4;
            this.login.Text = "login";
            this.login.TextChanged += new System.EventHandler(this.login_TextChanged_1);
            // 
            // inscription
            // 
            this.inscription.BackColor = System.Drawing.Color.Gray;
            this.inscription.FlatAppearance.BorderSize = 0;
            this.inscription.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.inscription.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inscription.ForeColor = System.Drawing.Color.White;
            this.inscription.Location = new System.Drawing.Point(390, 19);
            this.inscription.Name = "inscription";
            this.inscription.Size = new System.Drawing.Size(75, 29);
            this.inscription.TabIndex = 3;
            this.inscription.Text = "s\'inscrire";
            this.inscription.UseVisualStyleBackColor = false;
            this.inscription.Click += new System.EventHandler(this.inscription_Click);
            // 
            // connexion
            // 
            this.connexion.BackColor = System.Drawing.Color.Crimson;
            this.connexion.FlatAppearance.BorderSize = 0;
            this.connexion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.connexion.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connexion.ForeColor = System.Drawing.Color.White;
            this.connexion.Location = new System.Drawing.Point(307, 19);
            this.connexion.Name = "connexion";
            this.connexion.Size = new System.Drawing.Size(77, 29);
            this.connexion.TabIndex = 2;
            this.connexion.Text = "login";
            this.connexion.UseVisualStyleBackColor = false;
            this.connexion.Click += new System.EventHandler(this.connexion_Click);
            // 
            // menuHaut
            // 
            this.menuHaut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(245)))), ((int)(((byte)(66)))));
            this.menuHaut.Controls.Add(this.panel13);
            this.menuHaut.Dock = System.Windows.Forms.DockStyle.Top;
            this.menuHaut.Location = new System.Drawing.Point(0, 72);
            this.menuHaut.Name = "menuHaut";
            this.menuHaut.Size = new System.Drawing.Size(956, 60);
            this.menuHaut.TabIndex = 1;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(189)))), ((int)(((byte)(0)))));
            this.panel13.Controls.Add(this.paneldepot);
            this.panel13.Controls.Add(this.button4);
            this.panel13.Controls.Add(this.panel1);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(956, 60);
            this.panel13.TabIndex = 0;
            this.panel13.Paint += new System.Windows.Forms.PaintEventHandler(this.panel13_Paint);
            // 
            // paneldepot
            // 
            this.paneldepot.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(189)))), ((int)(((byte)(0)))));
            this.paneldepot.Controls.Add(this.validerdepot);
            this.paneldepot.Controls.Add(this.label1);
            this.paneldepot.Controls.Add(this.depot);
            this.paneldepot.Dock = System.Windows.Forms.DockStyle.Right;
            this.paneldepot.Location = new System.Drawing.Point(269, 0);
            this.paneldepot.Name = "paneldepot";
            this.paneldepot.Size = new System.Drawing.Size(394, 60);
            this.paneldepot.TabIndex = 4;
            this.paneldepot.Visible = false;
            this.paneldepot.Paint += new System.Windows.Forms.PaintEventHandler(this.paneldepot_Paint);
            // 
            // validerdepot
            // 
            this.validerdepot.BackColor = System.Drawing.Color.Crimson;
            this.validerdepot.FlatAppearance.BorderSize = 0;
            this.validerdepot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.validerdepot.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.validerdepot.ForeColor = System.Drawing.Color.White;
            this.validerdepot.Location = new System.Drawing.Point(279, 18);
            this.validerdepot.Name = "validerdepot";
            this.validerdepot.Size = new System.Drawing.Size(81, 29);
            this.validerdepot.TabIndex = 9;
            this.validerdepot.Text = "valider";
            this.validerdepot.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(3, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 21);
            this.label1.TabIndex = 8;
            this.label1.Text = "entrer depot :";
            // 
            // depot
            // 
            this.depot.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.depot.Location = new System.Drawing.Point(126, 16);
            this.depot.Name = "depot";
            this.depot.Size = new System.Drawing.Size(143, 29);
            this.depot.TabIndex = 7;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(189)))), ((int)(((byte)(0)))));
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(0, -3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(236, 60);
            this.button4.TabIndex = 3;
            this.button4.Text = "Acceuil";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(189)))), ((int)(((byte)(0)))));
            this.panel1.Controls.Add(this.textRecherche);
            this.panel1.Controls.Add(this.recherche);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(663, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(293, 60);
            this.panel1.TabIndex = 2;
            // 
            // textRecherche
            // 
            this.textRecherche.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRecherche.Location = new System.Drawing.Point(24, 18);
            this.textRecherche.Name = "textRecherche";
            this.textRecherche.Size = new System.Drawing.Size(143, 29);
            this.textRecherche.TabIndex = 6;
            this.textRecherche.Text = "textRecherche";
            // 
            // recherche
            // 
            this.recherche.BackColor = System.Drawing.Color.Crimson;
            this.recherche.FlatAppearance.BorderSize = 0;
            this.recherche.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.recherche.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recherche.ForeColor = System.Drawing.Color.White;
            this.recherche.Location = new System.Drawing.Point(189, 18);
            this.recherche.Name = "recherche";
            this.recherche.Size = new System.Drawing.Size(81, 29);
            this.recherche.TabIndex = 7;
            this.recherche.Text = "recherche";
            this.recherche.UseVisualStyleBackColor = false;
            this.recherche.Click += new System.EventHandler(this.recherche_Click);
            // 
            // contenu
            // 
            this.contenu.BackColor = System.Drawing.Color.DarkGray;
            this.contenu.Controls.Add(this.milieu);
            this.contenu.Controls.Add(this.menuDroite);
            this.contenu.Controls.Add(this.menuGauche);
            this.contenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contenu.Location = new System.Drawing.Point(0, 132);
            this.contenu.Name = "contenu";
            this.contenu.Size = new System.Drawing.Size(956, 416);
            this.contenu.TabIndex = 2;
            this.contenu.Paint += new System.Windows.Forms.PaintEventHandler(this.contenu_Paint);
            // 
            // milieu
            // 
            this.milieu.AutoScroll = true;
            this.milieu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.milieu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.milieu.Controls.Add(this.pictureBox1);
            this.milieu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.milieu.Location = new System.Drawing.Point(149, 0);
            this.milieu.Name = "milieu";
            this.milieu.Size = new System.Drawing.Size(594, 416);
            this.milieu.TabIndex = 4;
            this.milieu.Paint += new System.Windows.Forms.PaintEventHandler(this.milieu_Paint_1);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(592, 414);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // menuDroite
            // 
            this.menuDroite.BackColor = System.Drawing.Color.White;
            this.menuDroite.Controls.Add(this.validation);
            this.menuDroite.Controls.Add(this.menuDroiteContenu);
            this.menuDroite.Controls.Add(this.panel2);
            this.menuDroite.Dock = System.Windows.Forms.DockStyle.Right;
            this.menuDroite.ForeColor = System.Drawing.Color.Black;
            this.menuDroite.Location = new System.Drawing.Point(743, 0);
            this.menuDroite.Name = "menuDroite";
            this.menuDroite.Size = new System.Drawing.Size(213, 416);
            this.menuDroite.TabIndex = 1;
            // 
            // validation
            // 
            this.validation.AutoScroll = true;
            this.validation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(26)))), ((int)(((byte)(19)))));
            this.validation.Controls.Add(this.misetotalmultiple);
            this.validation.Controls.Add(this.gainpotentielmultiple);
            this.validation.Controls.Add(this.placementparis);
            this.validation.Controls.Add(this.label15);
            this.validation.Controls.Add(this.misetotalvaleur);
            this.validation.Controls.Add(this.misesomme);
            this.validation.Controls.Add(this.label16);
            this.validation.Dock = System.Windows.Forms.DockStyle.Top;
            this.validation.ForeColor = System.Drawing.Color.White;
            this.validation.Location = new System.Drawing.Point(0, 318);
            this.validation.Name = "validation";
            this.validation.Size = new System.Drawing.Size(213, 95);
            this.validation.TabIndex = 2;
            // 
            // misetotalmultiple
            // 
            this.misetotalmultiple.AutoSize = true;
            this.misetotalmultiple.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.misetotalmultiple.Location = new System.Drawing.Point(147, 40);
            this.misetotalmultiple.Name = "misetotalmultiple";
            this.misetotalmultiple.Size = new System.Drawing.Size(19, 17);
            this.misetotalmultiple.TabIndex = 6;
            this.misetotalmultiple.Text = "0 ";
            this.misetotalmultiple.Visible = false;
            // 
            // gainpotentielmultiple
            // 
            this.gainpotentielmultiple.AutoSize = true;
            this.gainpotentielmultiple.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gainpotentielmultiple.Location = new System.Drawing.Point(147, 16);
            this.gainpotentielmultiple.Name = "gainpotentielmultiple";
            this.gainpotentielmultiple.Size = new System.Drawing.Size(19, 17);
            this.gainpotentielmultiple.TabIndex = 5;
            this.gainpotentielmultiple.Text = "0 ";
            this.gainpotentielmultiple.Visible = false;
            // 
            // placementparis
            // 
            this.placementparis.BackColor = System.Drawing.Color.Crimson;
            this.placementparis.FlatAppearance.BorderSize = 0;
            this.placementparis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.placementparis.Location = new System.Drawing.Point(53, 60);
            this.placementparis.Name = "placementparis";
            this.placementparis.Size = new System.Drawing.Size(104, 23);
            this.placementparis.TabIndex = 4;
            this.placementparis.Text = "placer mon paris";
            this.placementparis.UseVisualStyleBackColor = false;
            this.placementparis.Click += new System.EventHandler(this.placementparis_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(7, 40);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(80, 17);
            this.label15.TabIndex = 0;
            this.label15.Text = "mise totale";
            // 
            // misetotalvaleur
            // 
            this.misetotalvaleur.AutoSize = true;
            this.misetotalvaleur.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.misetotalvaleur.Location = new System.Drawing.Point(120, 16);
            this.misetotalvaleur.Name = "misetotalvaleur";
            this.misetotalvaleur.Size = new System.Drawing.Size(19, 17);
            this.misetotalvaleur.TabIndex = 3;
            this.misetotalvaleur.Text = "0 ";
            // 
            // misesomme
            // 
            this.misesomme.AutoSize = true;
            this.misesomme.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.misesomme.Location = new System.Drawing.Point(120, 40);
            this.misesomme.Name = "misesomme";
            this.misesomme.Size = new System.Drawing.Size(19, 17);
            this.misesomme.TabIndex = 2;
            this.misesomme.Text = "0 ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(7, 16);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(104, 17);
            this.label16.TabIndex = 1;
            this.label16.Text = "gains potentiel";
            // 
            // menuDroiteContenu
            // 
            this.menuDroiteContenu.AutoScroll = true;
            this.menuDroiteContenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.menuDroiteContenu.Location = new System.Drawing.Point(0, 55);
            this.menuDroiteContenu.Name = "menuDroiteContenu";
            this.menuDroiteContenu.Size = new System.Drawing.Size(213, 263);
            this.menuDroiteContenu.TabIndex = 1;
            this.menuDroiteContenu.Paint += new System.Windows.Forms.PaintEventHandler(this.menuDroiteContenu_Paint);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(213, 55);
            this.panel2.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(189)))), ((int)(((byte)(0)))));
            this.panel7.Controls.Add(this.boutonmultiple);
            this.panel7.Controls.Add(this.boutonsimple);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.ForeColor = System.Drawing.Color.White;
            this.panel7.Location = new System.Drawing.Point(0, 34);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(213, 21);
            this.panel7.TabIndex = 1;
            this.panel7.Paint += new System.Windows.Forms.PaintEventHandler(this.panel7_Paint);
            // 
            // boutonmultiple
            // 
            this.boutonmultiple.FlatAppearance.BorderSize = 0;
            this.boutonmultiple.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.boutonmultiple.Location = new System.Drawing.Point(113, 0);
            this.boutonmultiple.Name = "boutonmultiple";
            this.boutonmultiple.Size = new System.Drawing.Size(77, 23);
            this.boutonmultiple.TabIndex = 1;
            this.boutonmultiple.Text = "multiple";
            this.boutonmultiple.UseVisualStyleBackColor = true;
            this.boutonmultiple.Visible = false;
            // 
            // boutonsimple
            // 
            this.boutonsimple.FlatAppearance.BorderSize = 0;
            this.boutonsimple.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.boutonsimple.Location = new System.Drawing.Point(10, 0);
            this.boutonsimple.Name = "boutonsimple";
            this.boutonsimple.Size = new System.Drawing.Size(77, 23);
            this.boutonsimple.TabIndex = 0;
            this.boutonsimple.Text = "simple";
            this.boutonsimple.UseVisualStyleBackColor = true;
            this.boutonsimple.Visible = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.panel6.Controls.Add(this.supprimertous);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(213, 34);
            this.panel6.TabIndex = 0;
            // 
            // supprimertous
            // 
            this.supprimertous.Location = new System.Drawing.Point(149, 8);
            this.supprimertous.Name = "supprimertous";
            this.supprimertous.Size = new System.Drawing.Size(52, 26);
            this.supprimertous.TabIndex = 15;
            this.supprimertous.Text = "suppr";
            this.supprimertous.UseVisualStyleBackColor = true;
            this.supprimertous.Click += new System.EventHandler(this.supprimertous_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(0, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 30);
            this.label4.TabIndex = 0;
            this.label4.Text = "Panier";
            // 
            // menuGauche
            // 
            this.menuGauche.BackColor = System.Drawing.Color.White;
            this.menuGauche.Dock = System.Windows.Forms.DockStyle.Left;
            this.menuGauche.Location = new System.Drawing.Point(0, 0);
            this.menuGauche.Name = "menuGauche";
            this.menuGauche.Size = new System.Drawing.Size(149, 416);
            this.menuGauche.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(0, 75);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(166, 69);
            this.button2.TabIndex = 4;
            this.button2.Text = "ligue1";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(0, 150);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(168, 69);
            this.button1.TabIndex = 3;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // sidepanel
            // 
            this.sidepanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(64)))), ((int)(((byte)(52)))));
            this.sidepanel.Location = new System.Drawing.Point(0, 0);
            this.sidepanel.Name = "sidepanel";
            this.sidepanel.Size = new System.Drawing.Size(21, 69);
            this.sidepanel.TabIndex = 1;
            this.sidepanel.Visible = false;
            this.sidepanel.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint_2);
            // 
            // football
            // 
            this.football.FlatAppearance.BorderSize = 0;
            this.football.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.football.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.football.Location = new System.Drawing.Point(0, 0);
            this.football.Name = "football";
            this.football.Size = new System.Drawing.Size(167, 69);
            this.football.TabIndex = 2;
            this.football.Text = "champion\'s ligue";
            this.football.UseVisualStyleBackColor = true;
            this.football.Click += new System.EventHandler(this.button2_Click);
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(956, 548);
            this.Controls.Add(this.contenu);
            this.Controls.Add(this.menuHaut);
            this.Controls.Add(this.header);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Paris en ligne";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.header.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.menuHaut.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.paneldepot.ResumeLayout(false);
            this.paneldepot.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.contenu.ResumeLayout(false);
            this.milieu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuDroite.ResumeLayout(false);
            this.validation.ResumeLayout(false);
            this.validation.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel header;
        private System.Windows.Forms.Panel menuHaut;
        private System.Windows.Forms.Panel contenu;
        private System.Windows.Forms.Panel sidepanel;
        private System.Windows.Forms.Panel menuGauche;
        private System.Windows.Forms.Button football;
        private System.Windows.Forms.Panel menuDroite;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.Button connexion;
        private System.Windows.Forms.Button inscription;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel validation;
        private System.Windows.Forms.Label misetotalvaleur;
        private System.Windows.Forms.Label misesomme;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button placementparis;
        private System.Windows.Forms.Button supprimertous;
        private System.Windows.Forms.Panel menuDroiteContenu;
        private System.Windows.Forms.TextBox motdepasselogin;
        private System.Windows.Forms.TextBox login;
        private System.Windows.Forms.Button deconnexion;
        private System.Windows.Forms.Button profil;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel milieu;
        private System.Windows.Forms.Button boutonmultiple;
        private System.Windows.Forms.Button boutonsimple;
        private System.Windows.Forms.Label misetotalmultiple;
        private System.Windows.Forms.Label gainpotentielmultiple;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button recherche;
        private System.Windows.Forms.TextBox textRecherche;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label argent;
        private System.Windows.Forms.Label solde;
        private System.Windows.Forms.Label soldetexte;
        private System.Windows.Forms.Panel paneldepot;
        private System.Windows.Forms.Button validerdepot;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox depot;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}

