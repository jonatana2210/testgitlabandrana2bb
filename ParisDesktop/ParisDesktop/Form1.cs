﻿using ParisDesktop.Model;
using ParisDesktop.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParisDesktop
{
    public partial class Form1 : Form
    {
        bool clickerASession = false;
        int i = 0;
        Panel panel = new Panel();
        List<Panel> panelDroite = new List<Panel>();
        List<Panel> panelMultipleDroite = new List<Panel>();
        //List<Panel> panelMilieu = new List<Panel>();
        string miseChange = "";
       
        public Form1()
        {
            InitializeComponent();

            
            sidepanel.Height = football.Height;
            sidepanel.Top = football.Top;
           
            //this.openMilieuForm(new Form1());
           
        }
        public string generateID()
        {
            return Guid.NewGuid().ToString("N");
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel1_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private async void Form1_Load(object sender, EventArgs e)
        {
            var liste =await ChampionnatApi.GetAll() ;
            List<Championnat> beau = ChampionnatApi.BeautifyJsonChampionnat(liste);
            for (int i = 0; i < beau.Count; i++)
            {
                
               Console.WriteLine(beau[i].nom+" nom des championnats");
                creerBouton(i, beau[i]);
            }
            
            
                
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Button bouton = (Button)sender;
            Console.WriteLine(bouton.Text);

            string[] idChampionnat = bouton.Name.Split('/');
            this.openMilieuForm(new Competition(idChampionnat[0],this));
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
        private void football_Click(object sender, EventArgs e)
        {
            sidepanel.Height = football.Height;
            sidepanel.Top = football.Top;
            
            if (activeForm!=null)
            activeForm.Close();
            else
            {
                Console.WriteLine(" null donc ne peut pas assigner");
            }
           
        }
        

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint_2(object sender, PaintEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {

        }
        private void supprimer_Click(object sender, EventArgs e)
        {
            
             
            Button bouton = (Button)sender;
            Panel p = (Panel)bouton.Parent;



            var item = panelDroite.Single(x => x.Name == p.Name);
            
            //Console.WriteLine(item.Controls.);
            //panelDroite.Remove(item);
            //panelDroite.Remove(panelDroite.Single(x => x.Name == p.Name));
            //Console.WriteLine(p.Name);
            if (menuDroiteContenu.Controls.Contains(p))
            {
                foreach (Control c in item.Controls.OfType<Button>().ToList())
                {
                    if (c.Text == "C")
                    {
                        
                        Console.WriteLine( c.Name);
                        Console.WriteLine((Button)this.Controls.Find(c.Name, true)[0]);
                        Button b =(Button)this.Controls.Find(c.Name, true)[0];
                        b.Enabled = true;
                        

                    }

                }
                foreach (Control b in p.Controls.OfType<Label>().ToList())
                {
                    if (b.Name.Contains("gain"))
                    {
                        
                        misetotalvaleur.Text = (Convert.ToDouble(misetotalvaleur.Text)- Convert.ToDouble(b.Text)).ToString();
                    }
                       
                }
                foreach (Control b in p.Controls.OfType<TextBox>().ToList())
                {
                    if (b.Name.Contains("misedefaut"))
                       misesomme.Text= (Convert.ToDouble(misesomme.Text) - Convert.ToDouble(b.Text)).ToString();
                }
                panelDroite.Remove(item);
                
                menuDroiteContenu.Controls.Remove(p);
                p.Dispose();
            }
            

            //panel26.Visible = false;

        }

        private void inscription_Click(object sender, EventArgs e)
        {
            this.openContenuForm(new Inscription());
        }

        private Panel creerPanel(Button b,Panel pnl, Match match,string idparis)
        {
            Random rnd = new Random();
            int month = rnd.Next(1, 125);
            string randomuniqueid = generateID();
            //prendre bouton parametre dans milieu
            Button boutoncote = new Button();
            boutoncote.Visible = false;
            boutoncote.Name = b.Name;
            boutoncote.Text = "C";

            //prendre panel dans milieu 
           
            string coteDansMilieu = null;
            foreach (Control c in pnl.Controls.OfType<Label>().ToList())
            {
                Console.WriteLine(c.Text+" labelllll ");
                coteDansMilieu = c.Text.ToString();

            }
                
            Console.WriteLine("debut creaation panel");
            Panel panel = new Panel();
            //creation bouton fermer
            Button boutonfermer = new Button();
            boutonfermer.Location = new System.Drawing.Point(143, -1);
            boutonfermer.Name = "boutonfermer";
            boutonfermer.Size = new System.Drawing.Size(30, 26);
            boutonfermer.TabIndex = 14;
            boutonfermer.Text = "X";
            boutonfermer.UseVisualStyleBackColor = true;
            boutonfermer.Click += new System.EventHandler(supprimer_Click);

            //creation boutonplus
            Button plusboutton1 = new Button();
            plusboutton1.BackColor = ColorTranslator.FromHtml("#ffcc00");
            plusboutton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            plusboutton1.ForeColor = System.Drawing.Color.Black;
            plusboutton1.Location = new System.Drawing.Point(106, 32);
            plusboutton1.Name = "plusboutton1"+ randomuniqueid;
            plusboutton1.Size = new System.Drawing.Size(35, 23);
            plusboutton1.TabIndex = 13;
            plusboutton1.Text = "+";
            plusboutton1.UseVisualStyleBackColor = false;
            plusboutton1.Click += new System.EventHandler(this.plusboutton_Click);

            //CREATION bouton moins
            Button moinsboutton1 = new Button();
            moinsboutton1.BackColor = ColorTranslator.FromHtml("#ffcc00");
            moinsboutton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            moinsboutton1.ForeColor = System.Drawing.Color.Black;
            moinsboutton1.Location = new System.Drawing.Point(37, 32);
            moinsboutton1.Name = "moinsboutton1" + randomuniqueid;
            moinsboutton1.Size = new System.Drawing.Size(35, 23);
            moinsboutton1.TabIndex = 12;
            moinsboutton1.Text = "-";
            moinsboutton1.UseVisualStyleBackColor = false;
            moinsboutton1.Click += new System.EventHandler(this.moinsboutton_Click);
            //CREATION misedefaut
            TextBox misedefaut1 = new TextBox();
            misedefaut1.BackColor = System.Drawing.Color.White;
            misedefaut1.Location = new System.Drawing.Point(69, 34);
            misedefaut1.Name = "misedefaut1" + randomuniqueid;
            misedefaut1.ReadOnly = true;
            misedefaut1.Size = new System.Drawing.Size(38, 20);
            misedefaut1.TabIndex = 10;
            misedefaut1.Text = "200";
            misedefaut1.TextChanged += new System.EventHandler(this.gain_TextChanged);

            //creation gain
            Label gain = new Label();
            gain.AutoSize = true;
            gain.Location = new System.Drawing.Point(135, 64);
            gain.Name = "gain" + randomuniqueid;
            gain.Size = new System.Drawing.Size(13, 13);
            gain.TabIndex = 9;
            gain.Text = (Convert.ToDouble(coteDansMilieu) * (Convert.ToDouble(misedefaut1.Text))).ToString();
            
            // cote
            // 
            Label cote = new Label();
            cote.AutoSize = true;
            cote.Location = new System.Drawing.Point(3, 64);
            cote.Name = "cotepris" + randomuniqueid;
            cote.Size = new System.Drawing.Size(22, 13);
            cote.TabIndex = 8;
            cote.Text = coteDansMilieu;

            //paris choisie
            Label parischoisi = new Label();
            parischoisi.AutoSize = true;
            parischoisi.Location = new System.Drawing.Point(70, 64);
            parischoisi.Name = "parispris" + randomuniqueid;
            parischoisi.Size = new System.Drawing.Size(22, 13);
            parischoisi.TabIndex = 8;
            parischoisi.Text = "paris:"+idparis;
            parischoisi.Visible = false;

            //paris choisie 2
            Label parischoisi2 = new Label();
            parischoisi2.AutoSize = true;
            parischoisi2.Location = new System.Drawing.Point(70, 64);
            parischoisi2.Name = "parisftsn" + randomuniqueid;
            parischoisi2.Size = new System.Drawing.Size(22, 13);
            parischoisi2.TabIndex = 8;
            parischoisi2.Text = "paris:" + b.Text;



            // 
            // label53
            // 
            Label label53 = new Label();
            label53.AutoSize = true;
            label53.Location = new System.Drawing.Point(140, 39);
            label53.Name = "label53" + randomuniqueid;
            label53.Size = new System.Drawing.Size(34, 13);
            label53.TabIndex = 7;
            label53.Text = "Gains";
            // 
            // label54
            // 
            Label label54 = new Label();
            label54.AutoSize = true;
            label54.Location = new System.Drawing.Point(3, 34);
            label54.Name = "label54" + randomuniqueid;
            label54.Size = new System.Drawing.Size(29, 13);
            label54.TabIndex = 6;
            label54.Text = "Cote";
            // 
            // label55
            // 
            Label label55 = new Label();
            label55.AutoSize = true;
            label55.Location = new System.Drawing.Point(100, 10);
            label55.Name = "1club" + randomuniqueid;
            label55.Size = new System.Drawing.Size(45, 13);
            label55.TabIndex = 5;
            label55.Text = match.equipe1.nom;
            // 
            // label56
            // 
            Label label56 = new Label();
            label56.AutoSize = true;
            label56.Location = new System.Drawing.Point(84, 10);
            label56.Name = "label56" + randomuniqueid;
            label56.Size = new System.Drawing.Size(10, 13);
            label56.TabIndex = 4;
            label56.Text = "-";
            // 
            // label57
            // 
            Label label57 = new Label();
            label57.AutoSize = true;
            label57.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label57.Location = new System.Drawing.Point(3, 9);
            label57.Name = "2club" + randomuniqueid;
            label57.Size = new System.Drawing.Size(75, 16);
            label57.TabIndex = 3;
            label57.Text = match.equipe2.nom;
            //creation panel 
            
            panel.Controls.Add(boutonfermer);
            panel.Controls.Add(parischoisi);
            panel.Controls.Add(parischoisi2);
            panel.Controls.Add(plusboutton1);
            panel.Controls.Add(moinsboutton1);
            panel.Controls.Add(misedefaut1);
            panel.Controls.Add(gain);
            panel.Controls.Add(cote);
            panel.Controls.Add(label53);
            panel.Controls.Add(label54);
            panel.Controls.Add(label55);
            panel.Controls.Add(label56);
            panel.Controls.Add(label57);
            panel.Controls.Add(boutoncote);
           //panel.Controls.Add(panelprisdansmilieu);
            panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
           
            //panel.Location = new System.Drawing.Point(6, 125);
            panel.Size = new System.Drawing.Size(175, 95);
            panel.TabIndex = 6;
            
            panel.Name = "panel1"+ randomuniqueid;
            //panel.ResumeLayout(false);
            //panel.PerformLayout();

            //this.menuDroiteContenu.Controls.Add(panel);

            return panel;

            Console.WriteLine("fin creaation panel ");
        }
        private void listerPanel()
        {
            double somme = 0;
            double misepersonne = 0;
            for (int i = 0; i < panelDroite.Count; i++)
            {
                //this.creerPanel();
                Console.WriteLine(panelDroite[i].Name + " liste nom panel");
                this.panelDroite[i].Location = new System.Drawing.Point(6, 125 * i);
                //this.panelDroite[i].ResumeLayout(false);
                //this.panelDroite[i].PerformLayout();
                this.menuDroiteContenu.Controls.Add(panelDroite[i]);

                foreach (Control b in panelDroite[i].Controls.OfType<Label>().ToList())
                {
                    if (b.Name.Contains("gain"))
                        somme += Convert.ToDouble(b.Text);
                }
                foreach (Control b in panelDroite[i].Controls.OfType<TextBox>().ToList())
                {
                    if (b.Name.Contains("misedefaut"))
                        misepersonne += Convert.ToDouble(b.Text);
                }

            }
            misetotalvaleur.Text = somme.ToString();
            misesomme.Text = misepersonne.ToString();

        }
        public async void cote1_Click(object sender, EventArgs e)
        {
            Button bouton = (Button)sender;
            Panel milieu = (Panel)bouton.Parent;
            string[] idMatch = bouton.Name.Split('/');
            var details = await MatchApi.GetMatchById(idMatch[0]);
            Match match = MatchApi.GetMatch(details);
            if (i == 0)
                {
                    Panel p=this.creerPanel(bouton,milieu,match,idMatch[1]);
                    panelDroite.Add(p);
                listerPanel();
                    //panelcote1.BackColor =Color.Green;
                    // label5.BackColor = Color.Red;
                    //cote1.BackColor = Color.Red;
                    //cote1.ForeColor = Color.Black;
                    // bouton.BackColor = Color.Red;
                    Console.WriteLine("clcké rouge creaation");
                    i++;

                    clickerASession = true;
                bouton.Enabled = false;
                }
            
            i = 0;
               /* else if (i == 1)
                {
                    //this.supprimer_Click(sender, e);
                    panelcote1.BackColor = Color.Green;
                    // label5.BackColor = Color.Green;
                    // cote1.BackColor =Color.ForestGreen;
                    //cote1.ForeColor = Color.Black;
                    //bouton.BackColor = Color.Green;
                    Console.WriteLine("clcké vert suppression");
                    clickerASession = false;

                    i = 0;
                }*/
            
            
            
        }

        private void creerBouton(int i,Championnat championnat)
        {
            Random rnd = new Random();
            int month = rnd.Next(1, 125);
            string randomuniqueid = generateID();
            Button boutongauche = new Button();
            boutongauche.FlatAppearance.BorderSize = 0;
            boutongauche.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            boutongauche.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            boutongauche.Location = new System.Drawing.Point(0, i*75);
            boutongauche.Name = championnat._id+"/"+ "boutonlien" + randomuniqueid;
            boutongauche.Size = new System.Drawing.Size(167, 69);
            boutongauche.TabIndex = 2;
            boutongauche.Text = championnat.nom ;
            boutongauche.UseVisualStyleBackColor = true;
            
            boutongauche.Click += new System.EventHandler(this.button2_Click);
            this.menuGauche.Controls.Add(boutongauche);
        }
        private void creerCardMilieu(int i)
        {
            Random rnd = new Random();
            int month = rnd.Next(1, 125);
            string randomuniqueid = generateID();
            // 
            // detailsMatch
            // 
            Button detailsMatch = new Button();
            detailsMatch.BackColor = System.Drawing.Color.Crimson;
            detailsMatch.FlatAppearance.BorderSize = 0;
            detailsMatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            detailsMatch.ForeColor = System.Drawing.Color.Black;
            detailsMatch.Location = new System.Drawing.Point(455, 6);
            detailsMatch.Name = "detailsMatch"+ randomuniqueid;
            detailsMatch.Size = new System.Drawing.Size(75, 23);
            detailsMatch.TabIndex = 13;
            detailsMatch.Text = "voir detail";
            detailsMatch.UseVisualStyleBackColor = false;
            detailsMatch.Click += new System.EventHandler(this.detailsMatch_Click);
            // 
            // label1
            // 
            Label label1 = new Label();
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(356, 14);
            label1.Name = "datematch"+ randomuniqueid;
            label1.Size = new System.Drawing.Size(65, 13);
            label1.TabIndex = 12;
            label1.Text = "05/05/2021";
            // 
            // panelcote2
            // 
            Panel panelcote2 = new Panel();
            panelcote2.BackColor = ColorTranslator.FromHtml("#ffcc00");

            panelcote2.Location = new System.Drawing.Point(366, 39);
            panelcote2.Name = "panelcote2"+ randomuniqueid;
            panelcote2.Size = new System.Drawing.Size(164, 40);
            panelcote2.TabIndex = 11;
            // 
            // label2
            // 
            
            Label label2 = new Label();
            label2.AutoSize = true;
            label2.ForeColor = System.Drawing.Color.Black;
            label2.Location = new System.Drawing.Point(121, 14);
            label2.Name = "valeurcote2a"+ randomuniqueid;
            label2.Size = new System.Drawing.Size(13, 13);
            label2.TabIndex = 7;
            label2.Text = "4";
            // 
            // cote2
            // 
            Button cote2 = new Button();
            cote2.BackColor = System.Drawing.Color.White;
            cote2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            cote2.Location = new System.Drawing.Point(0, 0);
            cote2.Name = "cote2"+ randomuniqueid;
            cote2.Size = new System.Drawing.Size(115, 40);
            cote2.TabIndex = 5;
            cote2.Text = "2";
            cote2.UseVisualStyleBackColor = false;
            cote2.Click += new System.EventHandler(this.cote1_Click);
            // 
            // panelcotenull
            // 
            Panel panelcotenull = new Panel();
            panelcotenull.BackColor = System.Drawing.Color.Green;
           
            panelcotenull.Location = new System.Drawing.Point(186, 39);
            panelcotenull.Name = "panelcotenull"+ randomuniqueid;
            panelcotenull.Size = new System.Drawing.Size(164, 40);
            panelcotenull.TabIndex = 10;
            // 
            // label3
            // 
            Label label3 = new Label();
            label3.AutoSize = true;
            label3.ForeColor = System.Drawing.Color.Black;
            label3.Location = new System.Drawing.Point(121, 14);
            label3.Name = "valeurcotenulla" + randomuniqueid;
            label3.Size = new System.Drawing.Size(13, 13);
            label3.TabIndex = 7;
            label3.Text = "2";
            // 
            // cotenull
            // 
            Button cotenull = new Button();
            cotenull.BackColor = System.Drawing.Color.White;
            cotenull.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            cotenull.Location = new System.Drawing.Point(0, 0);
            cotenull.Name = "cotenull"+ randomuniqueid;
            cotenull.Size = new System.Drawing.Size(115, 40);
            cotenull.TabIndex = 5;
            cotenull.Text = "X";
            cotenull.UseVisualStyleBackColor = false;
            cotenull.Click += new System.EventHandler(this.cote1_Click);
            // 
            // panelcote1
            //
            Panel panelcote1 = new Panel();
            panelcote1.BackColor = System.Drawing.Color.Green;
           
            panelcote1.Location = new System.Drawing.Point(3, 39);
            panelcote1.Name = "panelcote1"+ randomuniqueid;
            panelcote1.Size = new System.Drawing.Size(164, 40);
            panelcote1.TabIndex = 9;
            // 
            // label5
            // 
            Label label5 = new Label();
            label5.AutoSize = true;
            label5.ForeColor = System.Drawing.Color.Black;
            label5.Location = new System.Drawing.Point(121, 14);
            label5.Name = "valeurcote1a" + randomuniqueid;
            label5.Size = new System.Drawing.Size(13, 13);
            label5.TabIndex = 7;
            label5.Text = "1";
            // 
            // cote1
            // 
            Button cote1 = new Button();
            cote1.BackColor = System.Drawing.Color.White;
            cote1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            cote1.Location = new System.Drawing.Point(0, 0);
            cote1.Name = "cote1"+ randomuniqueid;
            cote1.Size = new System.Drawing.Size(115, 40);
            cote1.TabIndex = 5;
            cote1.Text = "1";
            cote1.UseVisualStyleBackColor = false;
            cote1.Click += new System.EventHandler(this.cote1_Click);
            // 
            // label6
            //
            Label label6 = new Label();
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(109, 16);
            label6.Name = "1club"+ randomuniqueid;
            label6.Size = new System.Drawing.Size(45, 13);
            label6.TabIndex = 4;
            label6.Text = "Chelsea";
            // 
            // label7
            // 
            Label label7 = new Label();
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(93, 16);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(10, 13);
            label7.TabIndex = 3;
            label7.Text = "-";
            // 
            // label8
            // 
            Label label8 = new Label();
            label8.AutoSize = true;
            label8.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label8.Location = new System.Drawing.Point(12, 15);
            label8.Name = "2club"+ randomuniqueid;
            label8.Size = new System.Drawing.Size(75, 16);
            label8.TabIndex = 2;
            label8.Text = "Real Madrid";
            panelcote2.Controls.Add(label2);
            panelcote2.Controls.Add(cote2);
            panelcotenull.Controls.Add(label3);
            panelcotenull.Controls.Add(cotenull);
            panelcote1.Controls.Add(label5);
            panelcote1.Controls.Add(cote1);
            Panel panelcard = new Panel();
            panelcard.Anchor = System.Windows.Forms.AnchorStyles.None;
            panelcard.AutoSize = true;
            panelcard.BackColor = System.Drawing.Color.White;
            panelcard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            panelcard.Controls.Add(detailsMatch);
            panelcard.Controls.Add(label1);
            panelcard.Controls.Add(panelcote2);
            panelcard.Controls.Add(panelcotenull);
            panelcard.Controls.Add(panelcote1);
            panelcard.Controls.Add(label6);
            panelcard.Controls.Add(label7);
            panelcard.Controls.Add(label8);
            panelcard.Location = new System.Drawing.Point(19, i*100);
            panelcard.Name = "cardmilieu"+ randomuniqueid;
            panelcard.Size = new System.Drawing.Size(549, 90);
            panelcard.TabIndex = 20;
            this.milieu.AutoScroll = true;
            this.milieu.Controls.Add(panelcard);
        }
      

        private void login_TextChanged(object sender, EventArgs e)
        {

        }

        private void gain_TextChanged(object sender, EventArgs e)
        {
            TextBox text = (TextBox)sender;
            Panel p = (Panel)text.Parent;
            Label gain = null;
            Label cote = null;
            double somme = 0;
            double misepersonne = 0;
            
            foreach (Control b in p.Controls.OfType<Label>().ToList())
            {   if (b.Name.Contains("gain"))
                    gain = (Label)b;
            }
            foreach (Control b in p.Controls.OfType<Label>().ToList())
            {
                if (b.Name.Contains("cote"))
                    cote = (Label)b;
            }
            

            Console.WriteLine(miseChange + "text niova ");
            text.Text = miseChange ;
            gain.Text = (Convert.ToDouble(miseChange)* Convert.ToDouble(cote.Text)).ToString();

            for (int i = 0; i < panelDroite.Count; i++)
            {

                Console.WriteLine(panelDroite[i].Name + " liste nom panel 2");
                foreach (Control b in panelDroite[i].Controls.OfType<Label>().ToList())
                {
                    if (b.Name.Contains("gain"))
                        somme += Convert.ToDouble(b.Text);
                }
                foreach (Control b in panelDroite[i].Controls.OfType<TextBox>().ToList())
                {
                    if (b.Name.Contains("misedefaut"))
                        misepersonne += Convert.ToDouble(b.Text);
                }

                //misedefaut

            }

            misetotalvaleur.Text = somme.ToString();
            misesomme.Text = misepersonne.ToString();

        }

        

        private void plusboutton_Click(object sender, EventArgs e)
        {
            Button bouton = (Button)sender;
            Panel p = (Panel)bouton.Parent;
           TextBox textbox = null;
            
            foreach (Control b in p.Controls.OfType<TextBox>().ToList())
            {
                textbox =(TextBox) b;
            }
           double mise = Convert.ToDouble(textbox.Text);
           Console.WriteLine(mise + " mise ");
           mise++;
            miseChange= mise.ToString();
            textbox.Text = miseChange;

        }

        private void moinsboutton_Click(object sender, EventArgs e)
        {
            Button bouton = (Button)sender;
            Panel p = (Panel)bouton.Parent;
            TextBox textbox = null;

            foreach (Control b in p.Controls.OfType<TextBox>().ToList())
            {
                textbox = (TextBox)b;
            }
            double mise = Convert.ToDouble(textbox.Text);
            if (mise < 201)
            {

            }
            else
            {
                mise--;
                
                miseChange = mise.ToString();
                textbox.Text = miseChange;
            }
            
           

        }

        private void button6_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint_3(object sender, PaintEventArgs e)
        {

        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel8_Paint(object sender, PaintEventArgs e)
        {

        }

        private void contenu_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }

        private void panel24_Paint(object sender, PaintEventArgs e)
        {

        }
        private Form activeForm = null;
        public void openMilieuForm(Form childForm)
        {
            if (activeForm != null)
                activeForm.Close();
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            childForm.AutoScroll = true;
            milieu.Controls.Add(childForm);
            milieu.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();

        }
        private void openContenuForm(Form childForm)
        {
            if (activeForm != null)
                activeForm.Close();
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            //childForm.AutoScroll = true;
            contenu.Controls.Add(childForm);
            contenu.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();

        }

        private void menuDroiteContenu_Paint(object sender, PaintEventArgs e)
        {
            
            


        }

        private void panel26_Paint(object sender, PaintEventArgs e)
        {

        }

        private void supprimertous_Click(object sender, EventArgs e)
        {
            Console.WriteLine(milieu.Name);
            Console.WriteLine(milieu.Controls.OfType<Button>());
            foreach (Panel c in panelDroite)
            {
                Console.WriteLine(c.Name);
                foreach (Control b in c.Controls.OfType<Button>().ToList())
                {
                    Console.WriteLine(b.Name);
                   

                      
                        Console.WriteLine((Button)this.Controls.Find(b.Name, true)[0]);
                        Button btn = (Button)this.Controls.Find(b.Name, true)[0];
                        btn.Enabled = true;


                    
                }

            }
            // menuDroiteContenu.Controls.Clear();
            foreach (Control c in menuDroiteContenu.Controls.OfType<Panel>().ToList())
            {
                menuDroiteContenu.Controls.Remove(c);
                c.Dispose();
            }
            
            panelDroite.Clear();
            misetotalvaleur.Text = "0";
            misesomme.Text = "0";
            Console.WriteLine("supprimer tous ");
        }

        public void detailsMatch_Click(object sender, EventArgs e)
        {
            Button bouton = (Button)sender;
            string[] idMatch = bouton.Name.Split('/');
            Panel p = (Panel)bouton.Parent;
            Console.WriteLine(p.Name+" panel details matchs");
            this.openMilieuForm(new DetailMatch(this,idMatch[0]));
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private async void placementparis_Click(object sender, EventArgs e)
        {

            if (LoginSession.LoggedIn == true)
            {
                if(LoginSession.soldeUtilisateur> Convert.ToDouble(misesomme.Text))
                {
                    var paniercreation = await PariApi.creerPanier(LoginSession.compte, DateTime.Now.ToString("MM/dd/yyyy"), "0", "0", "");
                    string idpanier = PariApi.creerPanier2(paniercreation);
                    Console.WriteLine("creation id panier" + idpanier);
                    var liste = await PariApi.GetPanierById(idpanier);
                    Panier panier = PariApi.GetPanier(liste);
                    Console.WriteLine(panier._id + " id panier please");
                    //supprimertous.Enabled = false;
                    Console.WriteLine(panelDroite.Count + " nombre de panel droite");
                    for (int i = 0; i < panelDroite.Count; i++)
                    {

                        Console.WriteLine(panelDroite[i].Name + " liste nom panel");

                        foreach (Control b in panelDroite[i].Controls.OfType<Button>().ToList())
                        {
                            if (b.Name.Contains("boutton"))
                                b.Enabled = false;
                            if (b.Name.Contains("boutonfermer"))
                                b.Enabled = false;
                        }
                        foreach (Control b in panelDroite[i].Controls.OfType<Label>().ToList())
                        {
                            if (b.Name.Contains("parispris"))
                            {

                                string[] parispris = b.Text.ToString().Split(':');
                                Console.WriteLine(" les id sont " + parispris[1]);
                                var pari1 = await PariApi.GetPariById(parispris[1]);
                                Pari unpari = PariApi.GetPari(pari1);
                                Panel mere = (Panel)b.Parent;
                                Console.WriteLine(mere.Name + " ----------- nom panel ");
                                TextBox valeurmise = mere.Controls.OfType<TextBox>().ToList()[0];
                                Console.WriteLine(valeurmise.Text + " ----------- valeur miseeee ");
                                var updatepari = await PariApi.modifierPari(unpari._id, valeurmise.Text);
                                unpari.mise = Convert.ToDouble(valeurmise.Text);
                                var panierparicreation = await PariApi.ajoutParisDansPanier(unpari, panier);
                                string idpanierpari = PariApi.ajoutParisDansPanier2(panierparicreation);

                                Console.WriteLine(" les id pari panier sont " + idpanierpari);
                            }

                        }


                    }
                    Console.WriteLine("-----------------------------------------------------------------");
                    Console.WriteLine("L'id panier sortie du boucle est " + panier._id);
                    Console.WriteLine("miseTotal " + misetotalvaleur.Text + " misesomme " + misesomme.Text);
                    if (misetotalvaleur.Text.Contains(','))
                    {
                        string[] conversion = misetotalvaleur.Text.Split(',');
                        string nouveau = conversion[0] + '.' + conversion[1];
                        Console.WriteLine(nouveau + " nouveu anle gain ");
                        var paniermodif = await PariApi.modifierPanier(panier._id, nouveau, misesomme.Text);
                        MessageBox.Show("Votre paris est enregistré ");
                        var decaissements = await CompteApi.Putdecaisser(LoginSession.UserID, misesomme.Text);
                        LoginSession.soldeUtilisateur = LoginSession.soldeUtilisateur - Convert.ToDouble(misesomme.Text);
                        solde.Text = LoginSession.soldeUtilisateur.ToString();
                        solde.Refresh();
                    }
                    else
                    {
                        var paniermodife = await PariApi.modifierPanier(panier._id, misetotalvaleur.Text, misesomme.Text);
                        //string message = PariApi.GetRetour(paniermodif);
                        Console.WriteLine(paniermodife + " valeur retour please ");
                        Console.WriteLine(misetotalvaleur.Text + " mise potentiel ");
                        Console.WriteLine(misesomme.Text + " mise total ");
                        //Console.WriteLine(message + " message ");

                        MessageBox.Show("Votre paris est enregistré ");
                        var decaissement = await CompteApi.Putdecaisser(LoginSession.UserID, misesomme.Text);
                        LoginSession.soldeUtilisateur = LoginSession.soldeUtilisateur - Convert.ToDouble(misesomme.Text);
                        solde.Text = LoginSession.soldeUtilisateur.ToString();
                        solde.Refresh();
                        //string message = PariApi.GetRetour(paniermodif);
                        //placementparis.Enabled = false;
                    }
                }
                else
                {
                    MessageBox.Show("vous n'avez pas assez d 'argent ");
                }
                


            }
            else
            {
                MessageBox.Show("vous devez vous connecter pour placer les paris ");
            }
            
            Console.WriteLine(misetotalvaleur.Text + " mise potentiel ");
            Console.WriteLine(misesomme.Text + " mise total ");
        }

        private void milieu_Paint(object sender, PaintEventArgs e)
        {
           
           
        }

        private void milieu_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void panel7_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void recherche_Click(object sender, EventArgs e)
        {
            string valeurarechercher = this.textRecherche.Text;
            this.openMilieuForm(new Recherche(this, valeurarechercher));
        }

        private void button4_Click_1(object sender, EventArgs e)
        {

        }

        private async void connexion_Click(object sender, EventArgs e)
        {
            string login = this.login.Text.ToString();
            string motDePasse = this.motdepasselogin.Text.ToString();

            var compte = await CompteApi.PostLogin(login,motDePasse);
            TokenCompte beau = CompteApi.GetTokenCompte(compte);

            Console.WriteLine(beau.auth+" authentification ");
            LoginSession.token = beau.token;
            Console.WriteLine(LoginSession.token + " authentification token ");
            if (beau.auth == true)
            {
                var compteutilisateur = await CompteApi.GetCompteByToken();
                Compte c = CompteApi.GetCompte(compteutilisateur);
                Console.WriteLine(c.solde + " SOLDE ");
                LoginSession.UserID = c._id;
                LoginSession.nomUtilisateur = c.nomUtilisateur;
                LoginSession.LoggedIn = beau.auth;
                LoginSession.soldeUtilisateur = c.solde;
                LoginSession.compte = c;
               
                //LoginSession.idpanier = idpanier;
                this.login.Visible = false;
                this.motdepasselogin.Visible = false;
                this.connexion.Visible = false;
                this.inscription.Visible = false;
                this.profil.Visible = true;
                this.profil.Text = LoginSession.nomUtilisateur;
                this.paneldepot.Visible = false;
                this.solde.Visible = true;
                this.solde.Text = LoginSession.soldeUtilisateur.ToString();
                this.soldetexte.Visible = true;
                this.argent.Visible = true;
                this.deconnexion.Visible = true;
            }
            else
            {
                //MessageBox.Show("login ou/et mot de passe erroné");
                MessageBox.Show(beau.message);
            }

            
        }

        private void profil_Click(object sender, EventArgs e)
        {
            this.openMilieuForm(new Historique(this));
        }

        private void deconnexion_Click(object sender, EventArgs e)
        {  
            this.login.Visible = true;
            this.motdepasselogin.Visible = true;
            this.connexion.Visible = true;
            this.inscription.Visible = true;
            this.profil.Visible = false;
            this.profil.Text = "";
            LoginSession.nomUtilisateur = "";
            LoginSession.LoggedIn = false;
            LoginSession.UserID = "";
            LoginSession.soldeUtilisateur =0;
            LoginSession.idpanier = "";
            this.deconnexion.Visible = false;
            this.paneldepot.Visible = false;
            this.solde.Visible = false;
            this.soldetexte.Visible = false;
            this.argent.Visible = false;
            //this.erreurconnexion.Visible = false;
            this.resetLoginForm();
        }
        private void resetLoginForm()
        {
            this.login.Text = "login";
            this.motdepasselogin.Text = "motdepasse";
        }

        private void panel13_Paint(object sender, PaintEventArgs e)
        {

        }

        private void paneldepot_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void login_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void solde_Click(object sender, EventArgs e)
        {

        }

        /* private void boutonsimple_Click(object sender, EventArgs e)
         {
             menuDroiteContenu.Controls.Clear();
             this.listerPanel();
             //gain potentiel
             gainpotentielmultiple.Visible = false;
             misetotalvaleur.Visible = true;
             //Mise totale
             misetotalmultiple.Visible = false;
             misesomme.Visible = true;
         }

         private void boutonmultiple_Click(object sender, EventArgs e)
         {
             menuDroiteContenu.Controls.Clear();
             //gain potentiel
             gainpotentielmultiple.Visible = true;
             misetotalvaleur.Visible = false;
             //Mise totale
             misetotalmultiple.Visible = true;
             misesomme.Visible = false;

         }*/
    }
}
