﻿namespace ParisDesktop
{
    partial class Historique
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelhistorique = new System.Windows.Forms.Panel();
            this.qrcodelabel = new System.Windows.Forms.Label();
            this.qrcodemagie = new System.Windows.Forms.PictureBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.fermerbutton = new System.Windows.Forms.Button();
            this.panierBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.championnatBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qrCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.miseTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gainTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qrCodeGenere = new System.Windows.Forms.DataGridViewImageColumn();
            this.voirdetails = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panelhistorique.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qrcodemagie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panierBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.championnatBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // panelhistorique
            // 
            this.panelhistorique.Controls.Add(this.qrcodelabel);
            this.panelhistorique.Controls.Add(this.qrcodemagie);
            this.panelhistorique.Controls.Add(this.dataGridView1);
            this.panelhistorique.Controls.Add(this.fermerbutton);
            this.panelhistorique.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelhistorique.Location = new System.Drawing.Point(0, 0);
            this.panelhistorique.Name = "panelhistorique";
            this.panelhistorique.Size = new System.Drawing.Size(540, 3000);
            this.panelhistorique.TabIndex = 0;
            // 
            // qrcodelabel
            // 
            this.qrcodelabel.AutoSize = true;
            this.qrcodelabel.Location = new System.Drawing.Point(450, 171);
            this.qrcodelabel.Name = "qrcodelabel";
            this.qrcodelabel.Size = new System.Drawing.Size(48, 13);
            this.qrcodelabel.TabIndex = 5;
            this.qrcodelabel.Text = "QRCode";
            // 
            // qrcodemagie
            // 
            this.qrcodemagie.Location = new System.Drawing.Point(428, 58);
            this.qrcodemagie.Name = "qrcodemagie";
            this.qrcodemagie.Size = new System.Drawing.Size(100, 90);
            this.qrcodemagie.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.qrcodemagie.TabIndex = 4;
            this.qrcodemagie.TabStop = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.qrCode,
            this.miseTotal,
            this.gainTotal,
            this.date,
            this.qrCodeGenere,
            this.voirdetails});
            this.dataGridView1.DataSource = this.panierBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(3, 37);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(393, 217);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // fermerbutton
            // 
            this.fermerbutton.Location = new System.Drawing.Point(0, 0);
            this.fermerbutton.Name = "fermerbutton";
            this.fermerbutton.Size = new System.Drawing.Size(67, 16);
            this.fermerbutton.TabIndex = 2;
            this.fermerbutton.Text = "X";
            this.fermerbutton.UseVisualStyleBackColor = true;
            this.fermerbutton.Click += new System.EventHandler(this.fermerbutton_Click_1);
            // 
            // panierBindingSource
            // 
            this.panierBindingSource.DataSource = typeof(ParisDesktop.Model.Panier);
            // 
            // championnatBindingSource
            // 
            this.championnatBindingSource.DataSource = typeof(ParisDesktop.Model.Championnat);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "_id";
            this.idDataGridViewTextBoxColumn.HeaderText = "_id";
            this.idDataGridViewTextBoxColumn.MinimumWidth = 2;
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.Width = 2;
            // 
            // qrCode
            // 
            this.qrCode.DataPropertyName = "qrCode";
            this.qrCode.HeaderText = "qrCode";
            this.qrCode.MinimumWidth = 2;
            this.qrCode.Name = "qrCode";
            this.qrCode.Width = 2;
            // 
            // miseTotal
            // 
            this.miseTotal.DataPropertyName = "miseTotal";
            this.miseTotal.HeaderText = "miseTotal";
            this.miseTotal.Name = "miseTotal";
            // 
            // gainTotal
            // 
            this.gainTotal.DataPropertyName = "gainTotal";
            this.gainTotal.HeaderText = "gainTotal";
            this.gainTotal.Name = "gainTotal";
            // 
            // date
            // 
            this.date.DataPropertyName = "date";
            this.date.HeaderText = "date";
            this.date.Name = "date";
            // 
            // qrCodeGenere
            // 
            this.qrCodeGenere.HeaderText = "QRCode";
            this.qrCodeGenere.Name = "qrCodeGenere";
            // 
            // voirdetails
            // 
            this.voirdetails.HeaderText = "Voir Details";
            this.voirdetails.Name = "voirdetails";
            this.voirdetails.Text = "Voir Details";
            this.voirdetails.UseColumnTextForButtonValue = true;
            // 
            // Historique
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMinSize = new System.Drawing.Size(0, 3000);
            this.ClientSize = new System.Drawing.Size(557, 338);
            this.Controls.Add(this.panelhistorique);
            this.Name = "Historique";
            this.Text = "Historique";
            this.Load += new System.EventHandler(this.Historique_Load);
            this.panelhistorique.ResumeLayout(false);
            this.panelhistorique.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qrcodemagie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panierBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.championnatBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelhistorique;
        private System.Windows.Forms.Button fermerbutton;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource championnatBindingSource;
        private System.Windows.Forms.BindingSource panierBindingSource;
        private System.Windows.Forms.Label qrcodelabel;
        private System.Windows.Forms.PictureBox qrcodemagie;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn qrCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn miseTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn gainTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn date;
        private System.Windows.Forms.DataGridViewImageColumn qrCodeGenere;
        private System.Windows.Forms.DataGridViewButtonColumn voirdetails;
    }
}