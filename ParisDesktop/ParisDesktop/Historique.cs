﻿using ParisDesktop.Model;
using ParisDesktop.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;

namespace ParisDesktop
{
    public partial class Historique : Form
    {
        Form1 form1;
        public Historique(Form1 form1)
        {
            this.form1 = form1;
            InitializeComponent();
        }
        
        private void fermerbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private async  void Historique_Load(object sender, EventArgs e)
        {
            var liste = await PariApi.GetPanierParCompte(LoginSession.UserID);
            List<Panier> beau = PariApi.getListePaniersParCompte(liste);

          

            dataGridView1.DataSource = beau;

        }

        private void fermerbutton_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
        e.RowIndex >= 0)
            {
                string val = dataGridView1.Rows[e.RowIndex].Cells[0].FormattedValue.ToString();
                Console.WriteLine(val + " io le valeur");
                // MessageBox.Show("be bibity "+value);
                form1.openMilieuForm(new DetailsHistorique(form1,val));
                
            }
           else if(senderGrid.Columns[e.ColumnIndex] is DataGridViewImageColumn &&
        e.RowIndex >= 0)
            {
                string value = dataGridView1.Rows[e.RowIndex].Cells[0].FormattedValue.ToString();
                Console.WriteLine(value + " io le valeur");
                 QRCoder.QRCodeGenerator QG = new QRCoder.QRCodeGenerator();
                 var myData = QG.CreateQrCode(value, QRCoder.QRCodeGenerator.ECCLevel.H);
                 var code = new QRCoder.QRCode(myData);
                qrcodemagie.Image = code.GetGraphic(50);
                //form1.openMilieuForm(new QRCodeVue(form1, value));


            }
           // if (dataGridView1.Columns[e.ColumnIndex].Name== "voirdetails")
           //{

            //}
        }

        

    }
}
