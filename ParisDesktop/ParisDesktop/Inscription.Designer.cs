﻿namespace ParisDesktop
{
    partial class Inscription
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelhautinscription = new System.Windows.Forms.Panel();
            this.fermerinscription = new System.Windows.Forms.Button();
            this.panelformulaireinscription = new System.Windows.Forms.Panel();
            this.inscrire = new System.Windows.Forms.Panel();
            this.emailinscription = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.inscriptionform = new System.Windows.Forms.Button();
            this.motdepasseinscription = new System.Windows.Forms.TextBox();
            this.motdepasse = new System.Windows.Forms.Label();
            this.nom = new System.Windows.Forms.Label();
            this.nominscription = new System.Windows.Forms.TextBox();
            this.panelhautinscription.SuspendLayout();
            this.panelformulaireinscription.SuspendLayout();
            this.inscrire.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelhautinscription
            // 
            this.panelhautinscription.BackColor = System.Drawing.Color.White;
            this.panelhautinscription.Controls.Add(this.fermerinscription);
            this.panelhautinscription.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelhautinscription.Location = new System.Drawing.Point(0, 0);
            this.panelhautinscription.Name = "panelhautinscription";
            this.panelhautinscription.Size = new System.Drawing.Size(614, 72);
            this.panelhautinscription.TabIndex = 0;
            // 
            // fermerinscription
            // 
            this.fermerinscription.Location = new System.Drawing.Point(12, 12);
            this.fermerinscription.Name = "fermerinscription";
            this.fermerinscription.Size = new System.Drawing.Size(55, 43);
            this.fermerinscription.TabIndex = 0;
            this.fermerinscription.Text = "X";
            this.fermerinscription.UseVisualStyleBackColor = true;
            this.fermerinscription.Click += new System.EventHandler(this.fermerinscription_Click);
            // 
            // panelformulaireinscription
            // 
            this.panelformulaireinscription.BackColor = System.Drawing.Color.White;
            this.panelformulaireinscription.Controls.Add(this.inscrire);
            this.panelformulaireinscription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelformulaireinscription.Location = new System.Drawing.Point(0, 72);
            this.panelformulaireinscription.Name = "panelformulaireinscription";
            this.panelformulaireinscription.Size = new System.Drawing.Size(614, 319);
            this.panelformulaireinscription.TabIndex = 1;
            // 
            // inscrire
            // 
            this.inscrire.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.inscrire.Controls.Add(this.emailinscription);
            this.inscrire.Controls.Add(this.label2);
            this.inscrire.Controls.Add(this.inscriptionform);
            this.inscrire.Controls.Add(this.motdepasseinscription);
            this.inscrire.Controls.Add(this.motdepasse);
            this.inscrire.Controls.Add(this.nom);
            this.inscrire.Controls.Add(this.nominscription);
            this.inscrire.Location = new System.Drawing.Point(85, 9);
            this.inscrire.Name = "inscrire";
            this.inscrire.Size = new System.Drawing.Size(409, 310);
            this.inscrire.TabIndex = 0;
            // 
            // emailinscription
            // 
            this.emailinscription.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailinscription.Location = new System.Drawing.Point(35, 117);
            this.emailinscription.Name = "emailinscription";
            this.emailinscription.Size = new System.Drawing.Size(305, 29);
            this.emailinscription.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(31, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 22);
            this.label2.TabIndex = 7;
            this.label2.Text = "email : ";
            // 
            // inscriptionform
            // 
            this.inscriptionform.BackColor = System.Drawing.Color.Crimson;
            this.inscriptionform.FlatAppearance.BorderSize = 0;
            this.inscriptionform.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.inscriptionform.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inscriptionform.ForeColor = System.Drawing.Color.White;
            this.inscriptionform.Location = new System.Drawing.Point(35, 256);
            this.inscriptionform.Name = "inscriptionform";
            this.inscriptionform.Size = new System.Drawing.Size(305, 46);
            this.inscriptionform.TabIndex = 4;
            this.inscriptionform.Text = "Inscription";
            this.inscriptionform.UseVisualStyleBackColor = false;
            this.inscriptionform.Click += new System.EventHandler(this.button1_Click);
            // 
            // motdepasseinscription
            // 
            this.motdepasseinscription.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.motdepasseinscription.Location = new System.Drawing.Point(35, 196);
            this.motdepasseinscription.Name = "motdepasseinscription";
            this.motdepasseinscription.PasswordChar = '*';
            this.motdepasseinscription.Size = new System.Drawing.Size(305, 29);
            this.motdepasseinscription.TabIndex = 3;
            // 
            // motdepasse
            // 
            this.motdepasse.AutoSize = true;
            this.motdepasse.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.motdepasse.Location = new System.Drawing.Point(31, 159);
            this.motdepasse.Name = "motdepasse";
            this.motdepasse.Size = new System.Drawing.Size(148, 22);
            this.motdepasse.TabIndex = 2;
            this.motdepasse.Text = "mot de passe : ";
            // 
            // nom
            // 
            this.nom.AutoSize = true;
            this.nom.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nom.Location = new System.Drawing.Point(31, 0);
            this.nom.Name = "nom";
            this.nom.Size = new System.Drawing.Size(66, 22);
            this.nom.TabIndex = 1;
            this.nom.Text = "nom : ";
            // 
            // nominscription
            // 
            this.nominscription.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nominscription.Location = new System.Drawing.Point(35, 38);
            this.nominscription.Name = "nominscription";
            this.nominscription.Size = new System.Drawing.Size(305, 29);
            this.nominscription.TabIndex = 0;
            this.nominscription.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // Inscription
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 391);
            this.Controls.Add(this.panelformulaireinscription);
            this.Controls.Add(this.panelhautinscription);
            this.Name = "Inscription";
            this.Text = "Inscription";
            this.panelhautinscription.ResumeLayout(false);
            this.panelformulaireinscription.ResumeLayout(false);
            this.inscrire.ResumeLayout(false);
            this.inscrire.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelhautinscription;
        private System.Windows.Forms.Panel panelformulaireinscription;
        private System.Windows.Forms.Panel inscrire;
        private System.Windows.Forms.TextBox motdepasseinscription;
        private System.Windows.Forms.Label motdepasse;
        private System.Windows.Forms.Label nom;
        private System.Windows.Forms.TextBox nominscription;
        private System.Windows.Forms.Button inscriptionform;
        private System.Windows.Forms.Button fermerinscription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox emailinscription;
    }
}