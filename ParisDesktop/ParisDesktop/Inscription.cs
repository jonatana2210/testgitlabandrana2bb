﻿using ParisDesktop.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParisDesktop
{
    public partial class Inscription : Form
    {
        public Inscription()
        {
            InitializeComponent();
        }

        private void fermerinscription_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private async void button1_Click(object sender, EventArgs e)
        {
            string nominscription = this.nominscription.Text.ToString() ;
            string emailinscription = this.emailinscription.Text.ToString();
            string motdepasseinscription = this.motdepasseinscription.Text.ToString();
            // double solde = Convert.ToDouble("4000");
            string solde = "7500";
            if(nominscription!="" && emailinscription!="" && motdepasseinscription != "")
            {
                var compte = await CompteApi.PostInscription(nominscription, emailinscription, solde, motdepasseinscription);
                bool beau = CompteApi.inscrire(compte);
                if (beau == true)
                {
                    MessageBox.Show("vous etes inscrit avec succes et vous obtenez un bonus ");
                    this.resetInscriptionForm();
                }
                else
                {
                    MessageBox.Show("quelque chose ne va pas");

                }
            }
            else
            {
                MessageBox.Show("quelque chose ne va pas");

            }

        }

        private void resetInscriptionForm()
        {
            this.nominscription.Text = "";
            this.emailinscription.Text = "";
            this.motdepasseinscription.Text = "";
        }
    }
}
