﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParisDesktop.Model
{
    public class Compte
    {
        public string _id { get; set; }

        public string nomUtilisateur { get; set; }
        public string email { get; set; }
        public string hash_motDePasse { get; set; }

        public  Double solde { get; set; }
    }
}
