﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParisDesktop.Model
{
    public static class LoginSession
    {
        public static bool LoggedIn;
        public static string UserID;
        public static string token;
        public static string nomUtilisateur;
        public static double soldeUtilisateur;
        public static string idpanier;
        public static Compte compte;
    }
}
