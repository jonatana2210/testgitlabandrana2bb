﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParisDesktop.Model
{
    public class Match
    {
        public string _id { get; set; }
        public Equipe equipe1 { get; set; }
        public Equipe equipe2 { get; set; }
        public int etat { get; set; }

        public DateTime date { get; set; }
        public string stade { get; set; }
        public string endroit { get; set; }
        public Championnat championnat { get; set; }

        public string status { get; set; }
    }
}
