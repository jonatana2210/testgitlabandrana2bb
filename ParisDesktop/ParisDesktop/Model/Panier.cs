﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParisDesktop.Model
{
    public class Panier
    {
        public string _id { get; set; }
        public Compte compte { get; set; }

        public DateTime date { get; set; }
        
        public double gainTotal { get; set; }

        public double miseTotal { get; set; }

        public string qrCode { get; set; }
        
    }
}
