﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParisDesktop.Model
{
   public class Pari
    {
        public string _id { get; set; }
        public Match match { get; set; }

        public Type type { get; set; }
        public string valeur { get; set; }
        public double cote { get; set; }
        public double mise { get; set; }
        public int resultat { get; set; }
    }
}
