﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParisDesktop.Model
{
    public class PariPanier
    {
        public string _id { get; set; }
        public Pari pari { get; set; }

        public Panier panier { get; set; }
        
    }
}
