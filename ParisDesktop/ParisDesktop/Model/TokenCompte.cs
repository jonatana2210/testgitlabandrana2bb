﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParisDesktop.Model
{
    public class TokenCompte
    {
        public bool auth { get; set; }
        public  string token { get; set; }

        public string message { get; set; }
    }
}
