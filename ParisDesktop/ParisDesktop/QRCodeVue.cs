﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParisDesktop
{
    public partial class QRCodeVue : Form
    {
        Form1 form1;
        string qrcode;
        public QRCodeVue(Form1 form1, string qrcode)
        {
            this.form1 = form1;
            this.qrcode = qrcode;
            InitializeComponent();
        }

        private void fermerbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
