﻿namespace ParisDesktop
{
    partial class Recherche
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelrecherche = new System.Windows.Forms.Panel();
            this.fermerbutton = new System.Windows.Forms.Button();
            this.panelrecherche.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelrecherche
            // 
            this.panelrecherche.AutoScroll = true;
            this.panelrecherche.AutoScrollMinSize = new System.Drawing.Size(0, 3000);
            this.panelrecherche.Controls.Add(this.fermerbutton);
            this.panelrecherche.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelrecherche.Location = new System.Drawing.Point(0, 0);
            this.panelrecherche.Name = "panelrecherche";
            this.panelrecherche.Size = new System.Drawing.Size(557, 338);
            this.panelrecherche.TabIndex = 0;
            // 
            // fermerbutton
            // 
            this.fermerbutton.Location = new System.Drawing.Point(0, 0);
            this.fermerbutton.Name = "fermerbutton";
            this.fermerbutton.Size = new System.Drawing.Size(67, 16);
            this.fermerbutton.TabIndex = 2;
            this.fermerbutton.Text = "X";
            this.fermerbutton.UseVisualStyleBackColor = true;
            this.fermerbutton.Click += new System.EventHandler(this.fermerbutton_Click);
            // 
            // Recherche
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(557, 338);
            this.Controls.Add(this.panelrecherche);
            this.Name = "Recherche";
            this.Text = "Recherche";
            this.Load += new System.EventHandler(this.Recherche_Load);
            this.panelrecherche.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelrecherche;
        private System.Windows.Forms.Button fermerbutton;
    }
}