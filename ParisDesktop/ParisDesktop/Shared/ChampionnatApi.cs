﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ParisDesktop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ParisDesktop.Shared
{
    public class ChampionnatApi
    {
        private static readonly string baseUrl = "https://winbet-api.herokuapp.com/api/championnats";

        public static async Task<string> GetAll()
        {
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage res = await client.GetAsync(baseUrl))
                {
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        if (data != null)
                        {
                            return data;
                        }
                    }
                }
            }
            return string.Empty;
        }
        public static List<Championnat> BeautifyJsonChampionnat(string jsonStr)
        {
            JArray parseJson = JArray.Parse(jsonStr);
           var users = parseJson.SelectToken("");
            List<Championnat> championnatList = JsonConvert.DeserializeObject<List<Championnat>>(users.ToString());
            //return parseJson.ToString(Formatting.Indented);
            return championnatList;
        }

        
    }
}
