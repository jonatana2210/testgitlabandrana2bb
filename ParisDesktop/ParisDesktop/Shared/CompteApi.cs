﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ParisDesktop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ParisDesktop.Shared
{
    public class CompteApi
    {
        private static readonly string baseUrl = "https://winbet-api.herokuapp.com/api/";

        public static async Task<string> PostLogin(string login, string motDePasse)
        {
            var inputData = new Dictionary<string, string>
            {
                {"email",login },
                {"motDePasse", motDePasse }

            };
            var input = new FormUrlEncodedContent(inputData);
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage res = await client.PostAsync(baseUrl+ "compte/login", input))
                {
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        if (data != null)
                        {
                            return data;
                        }
                    }
                }
            }
            return string.Empty;

        }
        public static TokenCompte GetTokenCompte(string jsonStr)
        {
            JObject parseJson = JObject.Parse(jsonStr);
            var users = parseJson.SelectToken("");
            TokenCompte userList = JsonConvert.DeserializeObject<TokenCompte>(users.ToString());
            //return parseJson.ToString(Formatting.Indented);
            return userList;
        }

        public static async Task<string> GetCompteByToken()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + LoginSession.token);
                using (HttpResponseMessage res = await client.GetAsync(baseUrl + "moi" ))
                {
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        if (data != null)
                        {
                            return data;
                        }
                    }
                }
            }
            return string.Empty;
        }
        public static Compte GetCompte(string jsonStr)
        {
            JObject parseJson = JObject.Parse(jsonStr);
            var users = parseJson.SelectToken("content");
            Compte userList = JsonConvert.DeserializeObject<Compte>(users.ToString());
            //return parseJson.ToString(Formatting.Indented);
            return userList;
        }

        public static async Task<string> PostInscription(string nom,string email,string solde, string motDePasse)
        {
            var inputData = new Dictionary<string, string>
            {
                {"nomUtilisateur",nom },
                {"email",email },
                {"solde",solde},
                {"motDePasse", motDePasse }

            };
            var input = new FormUrlEncodedContent(inputData);
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage res = await client.PostAsync(baseUrl + "compte/inscription", input))
                {
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        if (data != null)
                        {
                            return data;
                        }
                    }
                }
            }
            return string.Empty;

        }
        public static bool inscrire(string jsonStr)
        {
            JObject parseJson = JObject.Parse(jsonStr);
            var users = parseJson["etat"];
            Console.WriteLine(users);
            var result = users.ToObject<bool>();
            return result;
        }
        public static async Task<string> Putdecaisser(string id, string montant)
        {
            var inputData = new Dictionary<string, string>
            {
                {"id",id },
                {"montant", montant }

            };
            var input = new FormUrlEncodedContent(inputData);
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage res = await client.PutAsync(baseUrl + "compte/decaisser", input))
                {
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        if (data != null)
                        {
                            return data;
                        }
                    }
                }
            }
            return string.Empty;

        }
        public static string GetRetour(string jsonStr)
        {
            JObject parseJson = JObject.Parse(jsonStr);
            var retour = parseJson.SelectToken("message");
            Console.WriteLine(retour);
            var result = retour.ToObject<string>();
            return result;
        }
    }
}
