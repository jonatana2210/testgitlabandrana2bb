﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ParisDesktop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ParisDesktop.Shared
{
    public class MatchApi
    {

        private static readonly string baseUrl = "https://winbet-api.herokuapp.com/api/";
        public static async Task<string> GetMatchParChampionnat(string id)
        {
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage res = await client.GetAsync(baseUrl + "matchs/" + id))
                {
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        if (data != null)
                        {
                            return data;
                        }
                    }
                }
            }
            return string.Empty;
        }
        public static async Task<string> GetMatchById(string id)
        {
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage res = await client.GetAsync(baseUrl + "match/" + id))
                {
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        if (data != null)
                        {
                            return data;
                        }
                    }
                }
            }
            return string.Empty;
        }
        public static Match GetMatch(string jsonStr)
        {
            JObject parseJson = JObject.Parse(jsonStr);
            var match = parseJson.SelectToken("");
            Match unmatch = JsonConvert.DeserializeObject<Match>(match.ToString());
            //return parseJson.ToString(Formatting.Indented);
            return unmatch;
        }
        public static List<Match> BeautifyJsonMatchParChampionnat(string jsonStr)
        {
            JArray parseJson = JArray.Parse(jsonStr);
            var users = parseJson.SelectToken("");
            List<Match> matchListe = JsonConvert.DeserializeObject<List<Match>>(users.ToString());
            //return parseJson.ToString(Formatting.Indented);
            return matchListe;
        }
        public static async Task<string> rechercheSimple(string texte)
        {
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage res = await client.GetAsync(baseUrl + "matchs/recherche/" + texte))
                {
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        if (data != null)
                        {
                            return data;
                        }
                    }
                }
            }
            return string.Empty;
        }
    }
}
