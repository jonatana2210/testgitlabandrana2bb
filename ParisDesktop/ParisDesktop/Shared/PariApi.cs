﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ParisDesktop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ParisDesktop.Shared
{
    public class PariApi
    {
        private static readonly string baseUrl = "https://winbet-api.herokuapp.com/api/pari";
        private static readonly string baseUrl2 = "https://winbet-api.herokuapp.com/api/";
        public static async Task<string> GetPariParMatch(string id)
        {
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage res = await client.GetAsync(baseUrl2 + "paris/" + id))
                {
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        if (data != null)
                        {
                            return data;
                        }
                    }
                }
            }
            return string.Empty;
        }
        public static List<Pari> getListeParisParMatch(string jsonStr)
        {
            JArray parseJson = JArray.Parse(jsonStr);
            var paris = parseJson.SelectToken("");
            List<Pari> parisListe = JsonConvert.DeserializeObject<List<Pari>>(paris.ToString());
            //return parseJson.ToString(Formatting.Indented);
            return parisListe;
        }
        public static async Task<string> creerPanier(Compte compte,string date,string gainTotal,string miseTotal,string qrCode)
        {
           // string serialized = Newtonsoft.Json.JsonConvert.SerializeObject(compte);
            //StringContent stringContent = new StringContent(serialized, UnicodeEncoding.UTF8, "application/json");
            var inputData = new Dictionary<string,Object>
            {
                {"compte",compte },
                {"date",date },
                {"gainTotal",gainTotal },
                {"miseTotal",miseTotal },
                {"qrCode",qrCode },

            };

            StringContent sc = new StringContent(JsonConvert.SerializeObject(inputData), UnicodeEncoding.UTF8, "application/json");

            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage res = await client.PostAsync(baseUrl2 + "paniers",sc))
                {
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        if (data != null)
                        {
                            return data;
                        }
                    }
                }
            }
            return string.Empty;

        }
        public static string creerPanier2(string jsonStr)
        {
            JObject parseJson = JObject.Parse(jsonStr);
            var users = parseJson["message"];
            Console.WriteLine(users);
            var result = users.ToObject<string>();
            return result;
        }
        public static async Task<string> ajoutParisDansPanier(Pari pari, Panier panier)
        {
            var inputData = new Dictionary<string, Object>
            {
                {"pari", pari},
                {"panier",panier }

            };
            StringContent sc = new StringContent(JsonConvert.SerializeObject(inputData), UnicodeEncoding.UTF8, "application/json");

            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage res = await client.PostAsync(baseUrl2 + "pari_paniers", sc))
                {
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        if (data != null)
                        {
                            return data;
                        }
                    }
                }
            }
            return string.Empty;

        }
        public static string ajoutParisDansPanier2(string jsonStr)
        {
            JObject parseJson = JObject.Parse(jsonStr);
            var users = parseJson["message"];
            Console.WriteLine(users);
            var result = users.ToObject<string>();
            return result;
        }

        public static async Task<string> GetPanierById(string id)
        {
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage res = await client.GetAsync(baseUrl2 + "panier/" + id))
                {
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        if (data != null)
                        {
                            return data;
                        }
                    }
                }
            }
            return string.Empty;
        }
        public static Panier GetPanier(string jsonStr)
        {
            JObject parseJson = JObject.Parse(jsonStr);
            var panier = parseJson.SelectToken("");
            Panier panierun = JsonConvert.DeserializeObject<Panier>(panier.ToString());
            //return parseJson.ToString(Formatting.Indented);
            return panierun;
        }
        public static async Task<string> GetPariById(string id)
        {
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage res = await client.GetAsync(baseUrl2 + "pari/" + id))
                {
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        if (data != null)
                        {
                            return data;
                        }
                    }
                }
            }
            return string.Empty;
        }
        public static Pari GetPari(string jsonStr)
        {
            JObject parseJson = JObject.Parse(jsonStr);
            var pari = parseJson.SelectToken("");
            Pari unpari = JsonConvert.DeserializeObject<Pari>(pari.ToString());
            //return parseJson.ToString(Formatting.Indented);
            return unpari;
        }
        public static async Task<string> modifierPanier(string id, string gainTotal, string miseTotal)
        {
            var inputData = new Dictionary<string,string>
            {
                {"_id",id },
                {"gainTotal",gainTotal},
                {"miseTotal",miseTotal }

            };
            Console.WriteLine(inputData + " affichage donnee");
            Console.WriteLine(gainTotal.ToString() + " affichage donnee 2"+ miseTotal.ToString());
            var input = new FormUrlEncodedContent(inputData);
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage res = await client.PutAsync(baseUrl2 + "paniers",input))
                {
                    Console.WriteLine("entrain donnee");
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        if (data != null)
                        {
                            Console.WriteLine("entrain2 donnee METYYYYYYYYYYYYY"+data);
                            return data;
                        }
                    }
                }
            }
            Console.WriteLine("fin donnee");
            return string.Empty;

        }

        public static async Task<string> modifierPari(string id, string mise)
        {
            var inputData = new Dictionary<string, string>
            {
                {"_id",id },
              
                {"mise",mise }

            };
            Console.WriteLine(inputData + " affichage donnee");
            Console.WriteLine(mise + " affichage modification paris " );
            var input = new FormUrlEncodedContent(inputData);
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage res = await client.PutAsync(baseUrl2 + "paris", input))
                {
                    Console.WriteLine("entrain donnee");
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        if (data != null)
                        {
                            Console.WriteLine("entrain2 donnee METYYYYYYYYYYYYY" + data);
                            return data;
                        }
                    }
                }
            }
            Console.WriteLine("fin donnee");
            return string.Empty;

        }
        public static string GetRetour(string jsonStr)
        {
            JObject parseJson = JObject.Parse(jsonStr);
            var retour = parseJson.SelectToken("message");
            Console.WriteLine(retour);
            var result = retour.ToObject<string>();
            return result;
        }
        public static async Task<string> GetPanierParCompte(string id)
        {
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage res = await client.GetAsync(baseUrl2 + "paniers/compte/" + id))
                {
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        if (data != null)
                        {
                            return data;
                        }
                    }
                }
            }
            return string.Empty;
        }
        public static List<Panier> getListePaniersParCompte(string jsonStr)
        {
            JArray parseJson = JArray.Parse(jsonStr);
            var paris = parseJson.SelectToken("");
            List<Panier> panierListe = JsonConvert.DeserializeObject<List<Panier>>(paris.ToString());
            //return parseJson.ToString(Formatting.Indented);
            return panierListe;
        }
        public static async Task<string> GetPariParPanier(string id)
        {
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage res = await client.GetAsync(baseUrl2 + "pari_paniers/panier/" + id))
                {
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        if (data != null)
                        {
                            return data;
                        }
                    }
                }
            }
            return string.Empty;
        }
        public static List<PariPanier> getListePariParPanier(string jsonStr)
        {
            JArray parseJson = JArray.Parse(jsonStr);
            var paris = parseJson.SelectToken("");
            Console.WriteLine(paris);
            List<PariPanier> pariListe = JsonConvert.DeserializeObject<List<PariPanier>>(paris.ToString());
            //return parseJson.ToString(Formatting.Indented);
            return pariListe;
        }
    }
}
